<?php

namespace App\Providers;

use function Roots\view;
use Illuminate\Support\Facades\Log;
use Roots\Acorn\ServiceProvider;

class ThemeServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */


    public function getErrorTypeByValue($type)
    {
        $constants = get_defined_constants(true);

        foreach ($constants['Core'] as $key => $value) { // Each Core constant
            if (preg_match('/^E_/', $key)) { // Check error constants
                if ($type == $value) {
                    return $key;
                }

            }
        }
    }

    public function register()
    {
        $this->app->booted(
            function ($app) {
                set_error_handler(function ($level, $message, $file = '', $line = 0, $context = []) {
                    // Check if this error level is handled by error reporting
                    if (error_reporting() & $level) {
                        // Return false for any error levels that should be handled by the built in PHP error handler.
                        if ($level & (E_WARNING | E_NOTICE | E_DEPRECATED)) {
                            $error_label = ThemeServiceProvider::getErrorTypeByValue($level) ?? '';
                            $error_output = view('admin.error', [
                                'error_label' => $error_label,
                                'message'     => $message,
                                'file'        => $file,
                                'line'        => $line,
                            ]);

                            if (is_admin() && !wp_doing_ajax()) {
                                add_action('admin_footer', function () use ($error_output) {
                                    echo $error_output;
                                });
                            } elseif (!wp_doing_ajax()) {
                                add_action('footer', function () use ($error_output) {
                                    echo $error_output;
                                });
                            }

                            return false;
                        }

                        // Throw an exception to be handled by Laravel for all other errors.
                        throw new \ErrorException($message, 0, $level, $file, $line);
                    }
                });
            }
        );

    }


    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}