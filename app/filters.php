<?php

/**
 * Theme filters.
 */

namespace App;

/**
 * Add "… Continued" to the excerpt.
 *
 * @return string
 */
add_filter('excerpt_more', function () {
    return sprintf(' &hellip; <a href="%s">%s</a>', get_permalink(), __('Continued', 'sage'));
});

// ACF Google Maps
add_filter('acf/fields/google_map/api', function ($api) {
    $api['key'] = 'AIzaSyDPW5S46kSIniZJfhT_wqfNW_Y0r2d-wv4';
    return $api;
});

add_filter( 'gform_submit_button', '\App\\form_submit_button', 10, 2 );
function form_submit_button( $button, $form ) {
    return "<button class='button gform_button' id='gform_submit_button_{$form['id']}'><span>Submit</span></button>";
}