<?php

namespace App;

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});

add_action('admin_bar_menu', '\App\\my_remove_items', 999);
function my_remove_items($wp_admin_bar)
{
    $wp_admin_bar->remove_node('wp-logo');
    $wp_admin_bar->remove_node('comments');
    //$wp_admin_bar->remove_node('new-content');
    //$wp_admin_bar->remove_node('edit');
    $wp_admin_bar->remove_node('theme-dashboard');
    $wp_admin_bar->remove_node('customize');
    $wp_admin_bar->remove_node('new_draft');
    $wp_admin_bar->remove_node('wpseo-menu');
    $wp_admin_bar->remove_node('villatheme');
    //$wp_admin_bar->remove_node('updates');
}