<?php

namespace App\View\Composers;

use Roots\Acorn\View\Composer;
use Roots\Acorn\View\Composers\Concerns\AcfFields;
use WP_Query;

class App extends Composer
{
    use AcfFields;
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        '*',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'siteName'   => $this->siteName(),
            'title'      => $this->title(),
            'fields'     => collect($this->fields())->toArray(),
            'contact'    => $this->contactInfo(),
            'global'     => $this->global(),
            'featured_image' => $this->featuredImage(),
            'blog_posts'     => $this->blogPosts(),
            'featured_blog_post' => $this->featuredBlogPost(),
            'solutions'      => $this->solutions(),
            'solution_categories' => $this->solutionCategories(),
            'sectors'   	 => $this->sectors(),
            'challenges'   => $this->challenges(),
        ];
    }

    /**
     * Returns the site name.
     *
     * @return string
     */
    public function siteName()
    {
        return get_bloginfo('name', 'display');
    }

    public function featuredImage()
    {
    	global $post;
        if (is_home()) {
            $page_for_posts = get_option( 'page_for_posts' );
            $featured_image = get_the_post_thumbnail_url($page_for_posts);
        } else {
            $featured_image = wp_get_attachment_url( get_post_thumbnail_id());
        }
        return $featured_image;
    }

    public function global()
    {
        $global['cta_text']  		 = get_field('cta_text', 'option');
        $global['testimonials_text'] = get_field('testimonials_text', 'option');
        $global['testimonials']		 = get_field('testimonials', 'option');
        $global['our_impact_text']	 = get_field('our_impact_text', 'option');
        $global['impact_boxes'] 	 = get_field('impact_boxes', 'option');
        $global['why_us_text'] 		 = get_field('why_us_text', 'option');
        $global['why_us_icons'] 	 = get_field('why_us_icons', 'option');
        $global['sectors_text'] 	 = get_field('sectors_text', 'option');
        $global['what_we_do_text']   = get_field('what_we_do_text', 'option');
        $global['faqs']  			 = get_field('faqs', 'option');
        $global['statistics'] 		 = get_field('statistics', 'option');
        $global['challenge_text_1']  = get_field('challenge_text_1', 'option');
        $global['challenge_text_2']  = get_field('challenge_text_2', 'option');
        $global['challenge_footer_text']  = get_field('challenge_footer_text', 'option');
        $global['partners']  		 = get_field('partners', 'option');
        $global['brands']  	 		 = get_field('brands', 'option');
        $global['discovery_text']  	 = get_field('discovery_text', 'option');
        $global['discovery_boxes']   = get_field('discovery_boxes', 'option');
        return $global;
    }

    public function contactInfo()
    {
        $contact['email'] = get_field('email', 'option');
        $contact['phone'] = get_field('phone', 'option');
        $contact['twitter'] = get_field('twitter', 'option');
        $contact['facebook'] = get_field('facebook', 'option');
        $contact['instagram'] = get_field('instagram', 'option');
        $contact['linkedin'] = get_field('linkedin', 'option');
        $contact['opening_hours'] = get_field('opening_hours', 'option');
        $contact['address'] = get_field('address', 'option');
        $contact['second_address'] = get_field('second_address', 'option');
        return $contact;
    }

    public function featuredBlogPost()
    {
        $args = array(
            'post_type' => 'post',
            'posts_per_page' => 1,
            'orderby' => 'menu_order title',
            'order' => 'ASC',
        );
        $featured_blog_post = new WP_Query( $args );
        return $featured_blog_post;
    }

    public function blogPosts()
    {
        $args = array(
            'post_type' => 'post',
            'posts_per_page' => 10,
            'orderby' => 'menu_order title',
            'order' => 'ASC',
        );
        $blog_posts = new WP_Query( $args );
        return $blog_posts;
    }

    public function solutions()
    {
        $args = array(
            'post_type' => 'service',
            'posts_per_page' => -1,
            'orderby' => 'menu_order title',
            'order' => 'ASC',
        );
        $solutions = new WP_Query( $args );
        return $solutions;
    }

    public function solutionCategories()
    {
        $solution_categories = get_terms(array(
		    'taxonomy' => 'service-categories',
		    'hide_empty' => false,
		));
        return $solution_categories;
    }

    public function sectors()
    {
        $args = array(
            'post_type' => 'sector',
            'posts_per_page' => -1,
            'orderby' => 'menu_order title',
            'order' => 'ASC',
        );
        $sectors = new WP_Query( $args );
        return $sectors;
    }

    public function challenges()
    {
        $args = array(
            'post_type' => 'challenge',
            'posts_per_page' => -1,
            'orderby' => 'menu_order title',
            'order' => 'ASC',
        );
        $challenges = new WP_Query( $args );
        return $challenges;
    }

    public function challengeCategories()
    {
        $challenge_categories = get_terms(array(
		    'taxonomy' => 'challenge-categories',
		    'hide_empty' => false,
		));
        return $challenge_categories;
    }

    /**
     * Returns the post title.
     *
     * @return string
     */
    public function title()
    {
        if (is_tax()) {
            return single_cat_title('', false );
        }

        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }

        if ($this->view->name() !== 'partials.page-header') {
            return get_the_title();
        }

        if (is_archive()) {
            return get_the_archive_title();
        }

        if (is_search()) {
            /* translators: %s is replaced with the search query */
            return sprintf(
                __('Search Results for %s', 'sage'),
                get_search_query()
            );
        }

        if (is_404()) {
            return __('Not Found', 'sage');
        }

        return get_the_title();
    }
}
