<?php

/**
 * Theme setup.
 */

namespace App;

use function Roots\bundle;

use Walker_Nav_Menu;

/**
 * Register the theme assets.
 *
 * @return void
 */
add_action('wp_enqueue_scripts', function () {
    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    bundle('app')->enqueue()->localize('ajax_object', ['ajax_url' => admin_url('admin-ajax.php')]);
}, 100);

/**
 * Register the theme assets with the block editor.
 *
 * @return void
 */
add_action('enqueue_block_editor_assets', function () {
    if ($manifest = asset('scripts/manifest.asset.php')->get()) {
        wp_enqueue_script('sage/vendor.js', asset('scripts/vendor.js')->uri(), ...array_values($manifest));
        wp_enqueue_script('sage/editor.js', asset('scripts/editor.js')->uri(), ['sage/vendor.js'], null, true);

        wp_add_inline_script('sage/vendor.js', asset('scripts/manifest.js')->contents(), 'before');

        wp_localize_script('ajax-script', 'ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));
    }
}, 100);

/**
 * Register the initial theme setup.
 *
 * @return void
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil', [
        'clean-up',
        'nav-walker',
        'nice-search',
        'relative-urls'
    ]);

    add_filter('wpcf7_autop_or_not', '__return_false');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage'),
        'secondary_navigation' => __('Secondary Navigation', 'sage'),
    ]);

    add_theme_support('post-thumbnails');

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Add theme support for Wide Alignment
     * @link https://wordpress.org/gutenberg/handbook/designers-developers/developers/themes/theme-support/#wide-alignment
     */
    add_theme_support('align-wide');

    /**
     * Enable responsive embeds
     * @link https://wordpress.org/gutenberg/handbook/designers-developers/developers/themes/theme-support/#responsive-embedded-content
     */
    add_theme_support('responsive-embeds');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', [
        'caption',
        'comment-form',
        'comment-list',
        'gallery',
        'search-form',
        'script',
        'style'
    ]);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Enable theme color palette support
     * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/#block-color-palettes
     */
    add_theme_support('editor-color-palette', [
        [
            'name' => __('Primary', 'sage'),
            'slug' => 'primary',
            'color' => '#525ddc',
        ]
    ]);
}, 20);

//Dropdown walker
class Foundation_Walker extends Walker_Nav_Menu {

    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"vertical nested\" data-toggle>\n";
    }
}

/**
 * Register the theme sidebars.
 *
 * @return void
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ];

    register_sidebar([
        'name' => __('Primary', 'sage'),
        'id' => 'sidebar-primary'
    ] + $config);

    register_sidebar([
        'name' => __('Footer', 'sage'),
        'id' => 'sidebar-footer'
    ] + $config);
});

/**
 * Custom Post Types
 */
add_action('init', function () {

    register_post_type( 'service',
        array(
            'labels' => array(
                'name' => __( 'Services' ),
                'singular_name' => __( 'Service' )
            ),
            'public' => true,
            'has_archive' => false,
            'menu_position' => 20,
            'hierarchical' => true,
            'menu_icon' => 'dashicons-superhero',
            'supports' => array('thumbnail', 'title', 'editor'),
            'rewrite' => array( 'slug' => 'service', 'with_front' => false )
        )
    );

    register_taxonomy(
        'service-categories',
        'service',
        array(
            'label' => __( 'Service Categories' ),
            'hierarchical' => true,
            'publicly_queryable'  => true,
            'args' => array( 'orderby' => 'ASC' ),
            'supports' => array('thumbnail', 'title', 'editor'),
            'rewrite' => array( 'slug' => 'services/category' )
        )
    );

    register_post_type( 'sector',
        array(
            'labels' => array(
                'name' => __( 'Sectors' ),
                'singular_name' => __( 'Sector' )
            ),
            'public' => true,
            'has_archive' => false,
            'menu_position' => 20,
            'hierarchical' => true,
            'menu_icon' => 'dashicons-index-card',
            'supports' => array('thumbnail', 'title', 'editor'),
            'rewrite' => array( 'slug' => 'sector', 'with_front' => false )
        )
    );

    register_post_type( 'challenge',
        array(
            'labels' => array(
                'name' => __( 'Challenges' ),
                'singular_name' => __( 'Challenge' )
            ),
            'public' => true,
            'has_archive' => false,
            'menu_position' => 20,
            'hierarchical' => true,
            'menu_icon' => 'dashicons-portfolio',
            'supports' => array('thumbnail', 'title', 'editor'),
            'rewrite' => array( 'slug' => 'challenge', 'with_front' => false )
        )
    );

    register_taxonomy(
        'challenge-categories',
        'challenge',
        array(
            'label' => __( 'Challenge Categories' ),
            'hierarchical' => true,
            'publicly_queryable'  => true,
            'args' => array( 'orderby' => 'ASC' ),
            'rewrite' => array( 'slug' => 'challenges/category' )
        )
    );
});


/**
 * Custom ACF options
 */
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title'    => 'Global Content',
        'menu_title'    => 'Global Content',
        'menu_slug'     => 'global-content',
        'capability'    => 'edit_posts',
        'redirect'      => true
    ));

    acf_add_options_sub_page(array(
        'page_title'    => 'Content Blocks',
        'menu_title'    => 'Content Blocks',
        'parent_slug'   => 'global-content',
    ));

    acf_add_options_sub_page(array(
        'page_title'    => 'Contact Info',
        'menu_title'    => 'Contact Info',
        'parent_slug'   => 'global-content',
    ));
}

/*-----------------------------------------------------------------------------------*/
/* Moves Yoast SEO plugin to the bottom
/*-----------------------------------------------------------------------------------*/

add_filter( 'wpseo_metabox_prio', function () {
    return 'low';
});

/*-----------------------------------------------------------------------------------*/
/* Aye! Custom Login
/*-----------------------------------------------------------------------------------*/

add_action('login_enqueue_scripts', function () { ?>
    <style type="text/css">
        body {
            background-color: #000 !important;
            padding-top: 8% !important;
            overflow: hidden;
            font-family: 'F37 Ginger' !important;
            font-weight: 200;
        }
        #login {
            background-color: #fff;
            padding: 0 0 30px 0 !important;
            margin: 0 auto 0 !important;
            width: 355px !important;
        }
        #login h1 a, .login h1 a {
            background-image: url(<?= get_stylesheet_directory_uri(); ?>/public/images/logo.svg);
            background-position: left top !important;
            -webkit-background-size: 150px;
            background-size: 150px;
            height: 80px;
            width: 285px;
            margin-left: 35px !important;
            margin-right: 35px !important;
            margin-bottom: 0;
        }
        .login form {
            margin-top: 15px;
            margin-left: 0;
            padding: 0 35px 0 !important;
            background: #fff;
            -webkit-box-shadow: 0 1px 3px rgba(0,0,0,.13);
            box-shadow: none !important;
            border: none !important;
        }
        #backtoblog {
            position: absolute;
            left: 0;
            bottom: 20px;
        }
        .login #backtoblog a {
            color: #fff !important;
            font-size: 20px;
            font-weight: 200;
        }
        .login .button-primary {
            background: #2290DF !important;
            border: none !important;
            border-radius: 0 !important;
            display: block !important;
            width: 100% !important;
            text-shadow: none !important;
            height: 64px !important;
            line-height: 64px !important;
            margin: 24px 0 0 0 !important;
            font-size: 16px !important;
            text-transform: uppercase;
            letter-spacing: 2px;
        }
        .login #nav {
            margin: 12px 0 0 0 !important;
            padding: 0 35px !important;
        }
        .login label {
            color: #2290DF !important;
            text-transform: uppercase;
            letter-spacing: 1px;
            font-size: 13px !important;
        }
        .forgetmenot label {
            text-transform: none;
            color: #555d66 !important;
            letter-spacing: 0;
            font-weight: 200;
        }

        .login form .input, .login input[type=text] {
            box-shadow: none !important;
            height: 45px;
            margin-top: 7px !important;
            font-size: 21px !important;
            padding: 3px 10px !important;
        }
        .login form input[type=checkbox] {  box-shadow: none !important; }
        input[type=checkbox]:checked:before { color: #2290DF !important; }
        .login #login_error, .login .message {
            border-left: 4px solid #2290DF !important;
            padding: 12px;
            margin-left: 0;
            background-color: #f5f5f5 !important;
            box-shadow: none !important;
        }
        @font-face {
          font-family: 'F37 Ginger';
          src: url('https://aye.agency/wp-content/themes/aye/assets/fonts/F37Ginger/F37Ginger-Bold.eot'); /* IE9 Compat Modes */
          src: url('https://aye.agency/wp-content/themes/aye/assets/fonts/F37Ginger/F37Ginger-Bold.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
               url('https://aye.agency/wp-content/themes/aye/assets/fonts/F37Ginger/F37Ginger-Bold.woff') format('woff'), /* Modern Browsers */
               url('https://aye.agency/wp-content/themes/aye/assets/fonts/F37Ginger/F37Ginger-Bold.woff2') format('woff2'), /* Modern Browsers */
               url('https://aye.agency/wp-content/themes/aye/assets/fonts/F37Ginger/F37Ginger-Bold.ttf')  format('truetype'), /* Safari, Android, iOS */
               url('https://aye.agency/wp-content/themes/aye/assets/fonts/F37Ginger/F37Ginger-Bold.svg#7db0a735880422cc5fa2f936a390d651') format('svg'); /* Legacy iOS */
          font-style:   normal;
          font-weight:  700;
        }
        @font-face {
          font-family: 'F37 Ginger';
          src: url('https://aye.agency/wp-content/themes/aye/assets/fonts/F37Ginger/F37Ginger-Regular.eot'); /* IE9 Compat Modes */
          src: url('https://aye.agency/wp-content/themes/aye/assets/fonts/F37Ginger/F37Ginger-Regular.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
               url('https://aye.agency/wp-content/themes/aye/assets/fonts/F37Ginger/F37Ginger-Regular.woff') format('woff'), /* Modern Browsers */
               url('https://aye.agency/wp-content/themes/aye/assets/fonts/F37Ginger/F37Ginger-Regular.woff2') format('woff2'), /* Modern Browsers */
               url('https://aye.agency/wp-content/themes/aye/assets/fonts/F37Ginger/F37Ginger-Regular.ttf')  format('truetype'), /* Safari, Android, iOS */
               url('https://aye.agency/wp-content/themes/aye/assets/fonts/F37Ginger/F37Ginger-Regular.svg#cd0eb1181545a540be3acd13cd87c831') format('svg'); /* Legacy iOS */
          font-style:   normal;
          font-weight:  400;
        }
        @font-face {
          font-family: 'F37 Ginger';
          src: url('https://aye.agency/wp-content/themes/aye/assets/fonts/F37Ginger/F37Ginger-Light.eot'); /* IE9 Compat Modes */
          src: url('https://aye.agency/wp-content/themes/aye/assets/fonts/F37Ginger/F37Ginger-Light.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
               url('https://aye.agency/wp-content/themes/aye/assets/fonts/F37Ginger/F37Ginger-Light.woff') format('woff'), /* Modern Browsers */
               url('https://aye.agency/wp-content/themes/aye/assets/fonts/F37Ginger/F37Ginger-Light.woff2') format('woff2'), /* Modern Browsers */
               url('https://aye.agency/wp-content/themes/aye/assets/fonts/F37Ginger/F37Ginger-Light.ttf')  format('truetype'), /* Safari, Android, iOS */
               url('https://aye.agency/wp-content/themes/aye/assets/fonts/F37Ginger/F37Ginger-Light.svg#1c7eff18379a490689d240dabfae793f') format('svg'); /* Legacy iOS */
          font-style:   normal;
          font-weight:  200;
        }
    </style>
<?php });

