<?php

/**
 * Theme helpers.
 */

namespace App;

function getServiceIcon($post) {
	if($post) {
		$terms = get_the_terms($post->ID, 'service-categories');
		if($terms) {
			$term = $terms[0];
			$icon = get_field('icon', $term->taxonomy . '_' . $term->term_id);
		}
	}

	if(isset($icon)) {
		return $icon;
	}
}