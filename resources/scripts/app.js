/**
 * External Dependencies
 */
import 'jquery';
import 'is-in-viewport'
import {domReady} from '@roots/sage/client';

import Swiper, { Navigation, Pagination, EffectCoverflow, Autoplay } from 'swiper';

/**
 * app.main
 */
const main = async (err) => {
	if (err) {
		// handle hmr errors
		console.error(err);
	}

	jQuery(function($) {
		function swiper() {
			new Swiper('.home-swiper', {
				modules: [Navigation, Autoplay],
				loop: true,
				centeredSlides: true,
				autoplay: {
					delay: 5000,
				},
			});

			new Swiper('.testimonials-swiper', {
				modules: [Navigation, Pagination, EffectCoverflow, Autoplay],
				loop: true,
				speed: 600,
				effect: "coverflow",
				grabCursor: true,
				centeredSlides: true,
				spaceBetween: 50,
				autoplay: {
					delay: 5000,
				},
				slidesPerView: "auto",
				coverflowEffect: {
					rotate: 20,
					stretch: 0,
					depth: 50,
					modifier: 1,
					slideShadows: false,
				},
				navigation: {
					nextEl: '.testimonials-nav .next',
					prevEl: '.testimonials-nav .prev',
				},
				pagination: {
					el: '.testimonials-nav .swiper-pagination',
					type: 'bullets',
					clickable: true,
				},
			});


			new Swiper('.product-swiper', {
				modules: [Navigation, Autoplay],
				loop: true,
				speed: 600,
				slidesPerView: '1',
				spaceBetween: 15,
				autoplay: {
					delay: 5000,
				},
				breakpoints: {
					640: {
						spaceBetween: 20,
						slidesPerView: '2',
					},
					980: {
						spaceBetween: 30,
						slidesPerView: '3',
					},
					1040: {
						spaceBetween: 30,
						slidesPerView: '4',
					},
				},
				navigation: {
					nextEl: '.latest-products-nav .next',
					prevEl: '.latest-products-nav .prev',
				},
			});

			new Swiper('.card-swiper', {
				modules: [Navigation, Pagination, Autoplay],
				loop: false,
				speed: 600,
				slidesPerView: '1',
				grabCursor: true,
				spaceBetween: 15,
				autoplay: {
					delay: 5000,
				},
				breakpoints: {
					640: {
						spaceBetween: 20,
						slidesPerView: '2',
					},
					980: {
						spaceBetween: 30,
						slidesPerView: '3',
					},
				},
				pagination: {
					el: '.swiper-pagination',
					type: 'bullets',
					clickable: true,
				},
			});

			new Swiper('.discovery-swiper', {
				modules: [Navigation, Pagination, Autoplay],
				loop: false,
				speed: 600,
				slidesPerView: '1',
				grabCursor: true,
				spaceBetween: 15,
				autoplay: {
					delay: 5000,
				},
				navigation: {
					nextEl: '.nav .next',
					prevEl: '.nav .prev',
				},
				pagination: {
					el: '.swiper-pagination',
					type: 'bullets',
					clickable: true,
				},
			});

			var arr = [];
			$('[data-year]').each(function(){
				arr.push([ $(this).data('year') ]);
			});
			new Swiper('.history-swiper', {
				modules: [Navigation, Pagination, Autoplay],
				loop: true,
				speed: 600,
				spaceBetween: 30,
				autoHeight: true,
				simulateTouch: false,
				autoplay: {
					delay: 5000,
				},
				navigation: {
					nextEl: '.history-swiper .nav .next',
					prevEl: '.history-swiper .nav .prev',
				},
				pagination: {
					el: '.history-swiper .years-pagination',
					bulletActiveClass: 'active',
					bulletClass: 'year-bullet',
					clickable: true,
					renderBullet: function (index, className) {
						return '<span class="' + className + '">' + (arr[index]) + '</span>';
					},
				},
			});

			$(".standard-swiper").each(function(){
				var $this = $(this);
				new Swiper(this, {
					modules: [Navigation, Pagination, Autoplay],
					loop: true,
					speed: 600,
					autoHeight: true,
					grabCursor: true,
					spaceBetween: 30,
					autoplay: {
						delay: 5000,
					},
					touchEventsTarget: '.wrapper',
					navigation: {
						nextEl: $this.find('.nav .next')[0],
						prevEl: $this.find('.nav .prev')[0],
					},
					pagination: {
						el: $this.find('.swiper-pagination')[0],
						type: 'bullets',
						clickable: true,
					},
				});
			});

			new Swiper('.logo-swiper', {
				modules: [Navigation, Autoplay],
				loop: true,
				speed: 3000,
				slidesPerView: '2',
				autoplay: {
					delay: 0,
					disableOnInteraction: false,
				},
				breakpoints: {
					640: {
						slidesPerView: '3',
					},
					980: {
						slidesPerView: '3',
					},
					1040: {
						slidesPerView: '4',
					},
				},
				navigation: {
					nextEl: '.latest-products-nav .next',
					prevEl: '.latest-products-nav .prev',
				},
			});
		}

		function convertToSlug(Text) {
			return Text.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
		}

		function navigation() {
			$('.dot-toggle').on('click', function(e){
				e.preventDefault();
				$('.dot-toggle, .overlay-menu, body').toggleClass('is-active');
			});

			$('li.menu-item-has-children > a').on('click',function(event){
				event.preventDefault()

				$(this).parent().toggleClass('active');
				$(this).parent().find('ul').first().toggleClass('active');
				$(this).parent().siblings().removeClass('active');
				$(this).parent().siblings().find('ul').removeClass('active');

				//Hide menu when clicked outside
				$(this).parent().find('ul').mouseleave(function(){
					var thisUI = $(this);
					$('html').click(function(){
						thisUI.removeClass('active');
						$('html').unbind('click');
					});
				});
			});
		}

		function fixedHeader() {
			//Jump nav
			$('[data-anchor]').each(function(){
				var title = $(this).data('anchor');
				var slug = convertToSlug(title);

				$('.fixed-nav .menu').append('<li><a href="#'+ slug +'">'+ title +'</a></li>');
			});

			$('body').on('click', '.fixed-nav li a', function (e) {
				e.preventDefault();
				var href = $(this).text();

				$( 'html, body' ).animate({
					scrollTop: $('[data-anchor="'+href+'"]').offset().top,
				}, '300' );
			});

			var headerHeight = $('header').height() - $('.fixed-nav').height();
			$(window).scroll(function(){
				if ($(window).scrollTop() >= headerHeight) {
					$('.fixed-nav').addClass('fixed');
				}
				else {
					$('.fixed-nav').removeClass('fixed');
				}
			});
		}

		function getWeather() {
			$.ajax({
				url: 'https://api.weatherapi.com/v1/current.json?key=104ac7d2691a4aaea66111905222001&q=Cleckheaton&aqi=no',
				contentType: "application/json",
				dataType: 'json',
				success: function(result){
					$('.temp').html(result['current']['temp_c'] + '&#8451;');
					$('.condition').text(result['current']['condition']['text']);
				},
			});
		}

		function pageProgress() {
			if($('progress').length) {
				const win = $(window);
				const doc = $(document);
				const progressBar = $('progress');
				const setValue = () => win.scrollTop();
				const setMax = () => doc.height() - win.height();

				progressBar.attr({ value: setValue(), max: setMax() });

				doc.on('scroll', () => {
					progressBar.attr({ value: setValue() });
				});

				win.on('resize', () => {
					progressBar.attr({ value: setValue(), max: setMax() });
				})
			}
		}

		function tabs() {
			$('.tabs-js').each(function(){
				$(this).find('.tab-content').hide();
				$(this).find('.tab-content:first').show().addClass('tab-open');
				$(this).find('[data-tab]:first').addClass('active');
			});

			$('[data-tab]').click(function() {
				$(this).closest('.tabs-js').find('[data-tab]').removeClass('active');
				$(this).addClass('active');
				$(this).closest('.tabs-js').find('.tab-content').hide().removeClass('tab-open');

				var selectTab = $(this).attr('data-tab');
				$(selectTab).fadeIn().addClass('tab-open');
			});

			$('.tab-select').on('change', function() {
				$('.tab-content').hide();

				var selectTab = $(this).val();

				$(selectTab).fadeIn();
			});
		}

		function accordion() {
			$('.accordion-title').click(function(j) {
				var dropDown = $(this).closest('.accordion').find('.accordion-content');

				$(this).closest('#accordion').find('.accordion-content').not(dropDown).slideUp();

				if ($(this).hasClass('open')) {
					$(this).removeClass('open');
				} else {
					console.log('test');
					$(this).closest('#accordion').find('.accordion-title.open').removeClass('open');
					$(this).addClass('open');
				}

				dropDown.stop(false, true).slideToggle();

				j.preventDefault();
			});
		}

		function videoBlock() {
			$('.video-block #play-video').on('click', function(e){
				e.preventDefault();

				$(this).parent().find('.video').fadeIn();
				$(this).parent().find('.video iframe')[0].src += "&autoplay=1";
			});
		}


		function modal() {
			$('[data-open]').on('click', function(){
				var id = $(this).data('open');
				$('body').toggleClass('modal-open');

				$('#'+id).toggleClass('active');
			});

			$('body').on('click', '.modal .close, .continue', function () {
				$('.modal.active').removeClass('active');
				$('body').removeClass('modal-open');
			});
		}

		function portal() {
			if (window.matchMedia('(max-width: 767px)').matches) {
				if($('.portal .sidebar').length) {
					$(".portal .sidebar li.active").prependTo(".portal .sidebar ul");

					$(document).on('click', ".portal .sidebar", function () {
						$('.portal .sidebar').toggleClass('open');
						$('.portal .sidebar').parent().css({'position': 'relative', 'z-index': '1'});
					});
				}
			}
		}

		function isVisible() {
			viewportDetection();

			$( window ).scroll( function() {
				viewportDetection();
			});

			function viewportDetection() {
				$( '[data-viewport="detect"]:in-viewport' ).addClass( 'visible' );
			}
		}

		function load_more() {
			$(document).on('click', ".load-more a", function (e) {
				e.preventDefault();
				var link = $(this).attr('href');
				$('.load-more a').text('Loading...');

				$.get(link, function(data) {
					var post = $("#posts .post-item ", data);
					$('#posts').append(post);
				});

				$('.load-more').load(link+' .load-more a');
			});
		}


		$(document).ready(() => {
			swiper();
			navigation();
			accordion();
			modal();
			tabs();
			isVisible();
			load_more();
			fixedHeader();
			pageProgress();
			getWeather();
			videoBlock();
			portal();
		});
	});
};

/**
 * Initialize
 *
 * @see https://webpack.js.org/api/hot-module-replacement
 */
domReady(main);
import.meta.webpackHot?.accept(main);
