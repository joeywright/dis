<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')

  <body @php body_class() @endphp>

  	@php if (is_user_logged_in()) { @endphp
	  	@include('partials.header-portal')
	  	@php do_action('get_header') @endphp

	    <main class="portal" data-anchor="Home">
	        @yield('content')
	    </main>
	@php } else {
		wp_redirect('/portal-login/');
	} @endphp

    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp

  </body>
</html>
