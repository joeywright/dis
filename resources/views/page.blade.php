@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

	@include('partials.hero')

	@if( '' !== get_post()->post_content )
		<section data-viewport="detect" data-animate="fade" data-anchor="Introduction">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
					<div class="large-8 medium-10 small-12 cell">
						@if(isset($fields['intro_text']))
							@if($fields['intro_text'])
								{!! $fields['intro_text'] !!}
							@endif
						@endif
					</div>
				</div>
				<div class="grid-x grid-margin-x mt4">
					<div class="large-6 large-offset-4 medium-8 small-12 cell">
						@php the_content(); @endphp
					</div>
				</div>
			</div>
		</section>
	@endif

	@include('partials.flexible-content')

	@include('partials.cta')

  @endwhile
@endsection
