@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

	<div class="hero" data-viewport="detect" data-animate="fade" id="Prominent Brands">
		<div class="grid-container">
			<div class="grid-x grid-margin-x align-middle">
				<div class="large-9 medium-10 small-12 cell">
					@if(isset($fields['subheading']) && $fields['subheading'])
						<h5>{!! $fields['subheading'] !!}</h5>
					@endif
					<h2 style="color:#fff">{!! $title !!}</h2>
				</div>
			</div>
		</div>
	</div>

	@include('partials.trusted-by')

	@if( '' !== get_post()->post_content )
		<section data-viewport="detect" data-animate="fade">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
					<div class="large-8 medium-10 small-12 cell">
						@php the_content(); @endphp
					</div>
				</div>
			</div>
		</section>
	@endif

	@include('partials.flexible-content')

	<section data-viewport="detect" data-animate="fade" data-anchor="Our History">
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="large-8 medium-10 small-12 cell">
					<h5>DIS through the years</h5>
					<h1>Our History</h1>
				</div>
			</div>
		</div>
	</section>

	@if($fields['our_history'])
		<section data-viewport="detect" data-animate="fade">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
		    		<div class="cell">

						<div class="history-swiper swiper">
		    				<div class="years-pagination"></div>

			           		<div class="swiper-wrapper">
			           			@foreach ($fields['our_history'] as $key=>$year)
									<div class="swiper-slide">
										<div class="grid-x grid-margin-x align-middle">
											<div class="large-6 medium-6 small-12 cell">
												@if($year['image'])
											  		<img src="{{ $year['image']['url'] }}" alt="{{ $year['image']['alt'] }}" class="mb2">
										  		@endif
										  	</div>
											<div class="large-5 large-offset-1 medium-6 small-12 cell">
										  		@if($year['year'])
													<h2 data-year="{!! $year['year'] !!}" class="primary-text">{!! $year['year'] !!}</h2>
										  		@endif

										  		@if($year['text'])
													<div class="content">{!! $year['text'] !!}</div>
										  		@endif

								            	<div class="nav">
						                            <div class="prev"><img src="@asset('images/arrow.svg')" width="50"></div>
													<div class="next"><img src="@asset('images/arrow.svg')" width="50"></div>
						                        </div>
										  	</div>
										</div>
									</div>
								@endforeach
			            	</div>
			            </div>
		            </div>
	        	</div>
			</div>
		</section>
	@endif

	@include('partials.partners')

	@include('partials.cta')

  @endwhile
@endsection
