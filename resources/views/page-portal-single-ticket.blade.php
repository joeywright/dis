{{--
  Template Name: Portal – Single Ticket
--}}
@extends('layouts.portal')

@section('content')
  @while(have_posts()) @php the_post() @endphp

	@include('partials.hero-portal')

	<section data-viewport="detect" data-animate="fade" data-anchor="Introduction">
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="large-3 medium-4 small-12 cell">
					@include('partials.sidebar-portal')
				</div>
				<div class="large-8 large-offset-1 medium-8 small-12 cell">
					@php
						$id 	  = htmlspecialchars($_GET['id']);
						$case 	  = htmlspecialchars($_GET['case']);
						$records  = getSupportCase($case);
						$messages = getCaseMessages($id, $case);
						$company  = getCurrentCompany();
					@endphp

					@if($records)
						@foreach($records as $record)
							@foreach($record as $case)
								<h2>{{ $case->title }}</h2>
								<div class="ticket-overview">
									<h3>Ticket Overview</h3>
									<div>
										<span>No.</span>
										{{ $case->caseNumber }}
									</div>
									<div>
										<span>Date</span>
										{{ date("d/m/Y", strtotime($case->createdDate)); }}
									</div>
									@if($case->category)
										<div>
											<span>Case Type</span>
											{{ $case->category->name }}
										</div>
									@endif
									@if($case->status)
										<div>
											<span>Status</span>
											{{ $case->status->name }}
										</div>
									@endif
								</div>
							@endforeach
						@endforeach
					@endif

					@if($messages)
						@foreach($messages as $case_message)
							<div class="message">
								<div class="message-title">
									<h4>{{ $case_message->dateTime }}</h4>
									<p>{{ $case_message->author->name }}</p>
								</div>
								<div class="message-box">
									@php
										$tags = array(
											'br' => array(),
											'p' => array(),
											'strong' => array(),
											'blockquote' => array(),
											'a' => array(),
										);
										$string = str_replace('Copyright material and/or confidential and/or privileged information may be contained in this e-mail and any attachments. The material and information are intended for the use of the intended addressee only. If you are not the intended addressee, or the person responsible for delivering it to the intended addressee, you may not copy, disclose, distribute, disseminate or deliver it to anyone else or use it in any unauthorised manner or take or omit to take any action in reliance on it. To do so is prohibited and may be unlawful. The views expressed in this e-mail may not be official policy but the personal views of the originator. If you receive this e-mail in error, please advise the sender immediately by using the reply facility in your e-mail software. Please also delete this e-mail and all documents attached immediately.', '', $case_message->message);

										echo wp_kses($string, $tags);
									@endphp
								</div>
							</div>
						@endforeach
					@endif

					<script type="text/javascript">
						jQuery(function($) {
							$(document).ready(() => {
								$(document).on('submit', '.submit-reply', function(e) {
									e.preventDefault();
									var errors = 0;

					            	var title = $('input[name="title"]').val();
									var message = $('textarea[name="message"]').val();
									var caseno = $('input[name="caseno"]').val();
									var id = $('input[name="id"]').val();
									var company = $('input[name="company"]').val();

									$('.form-message').html('');

									if(title == '') {
										errors = 1;
										$('.form-message').append('<p>Please enter a subject.</p>');
									}
									if(message == '') {
										errors = 1;
										$('.form-message').append('<p>Please enter a message.</p>');
									}

									if(errors == 0) {
										$.ajax({
									        url: ajax_object.ajax_url,
									        type: 'GET',
									        data: {
									            'action': 'submit_reply',
									            'title':  title,
									            'message': message,
									            'case': caseno,
									            'id': id,
									            'company': company
									        },
									        beforeSend: function() {
									        	$('.loading').show();
									        },
									        success: function(data) {
									    		$('.form-message').html(data);
									        	$('.loading').hide();
									        	setTimeout(function(){
												    window.location.reload(); // you can pass true to reload function to ignore the client cache and reload from the server
												}, 3000);
											},
											error: function(MLHttpRequest, textStatus, errorThrown){
												console.log(errorThrown);
									        	$('.loading').hide();
											},
									    });
									}
							    });
				            });
			            });
					</script>

					<h3>Reply to ticket</h3>
					<form class="submit-reply mt3">
						<label>
							Subject *
							<input type="text" name="title">
						</label>
						<label>
							Message *
							<textarea name="message" rows="4"></textarea>
						</label>

						<input type="hidden" value="{{ $id }}" name="id">
						<input type="hidden" value="{{ $company->internalId }}" name="company">
						<input type="hidden" value="{{ $_GET['case'] }}" name="caseno">

						<div class="loading" style="display: none;"><img src="@asset('images/portal/loading.svg')" width="40"></div>
						<div class="form-message"></div>
						<button type="submit" class="button">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</section>

	@include('partials.flexible-content')

  @endwhile
@endsection
