{{--
  Template Name: Portal – Home
--}}
@extends('layouts.portal')

@section('content')
  @while(have_posts()) @php the_post() @endphp

	@include('partials.hero-portal')

	@if( '' !== get_post()->post_content )
		<section data-viewport="detect" data-animate="fade" data-anchor="Introduction">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
					<div class="large-3 medium-4 small-12 cell">
						@include('partials.sidebar-portal')
					</div>
					<div class="large-8 large-offset-1 medium-8 small-12 cell">
						@php
							if(isset($_SESSION['first_name'])):
								$first_name = htmlspecialchars($_SESSION['first_name']);
							else:
								if (is_user_logged_in()):
									$new_user = get_userdata(get_current_user_id());
									$first_name = $new_user->first_name;
								endif;
							endif;

							if(isset($first_name)):
								echo '<h3>Hello '.$first_name.',</h3>';
							endif;

						the_content(); @endphp

						<hr class="mt4 mb4">

						@if(is_user_logged_in())
							@php $services = get_field('services_in_use', 'user_'. get_current_user_id()); @endphp
							@if($services)
								<h2 class="m0">Services You're Using</h2>
								<div class="grid-x grid-margin-x large-up-3 medium-up-2 small-up-1 mt3 hide-for-small-only">
									@foreach($services as $service)
										@php
											$service_icon = App\getServiceIcon($service);
										@endphp
										<div class="cell mb3">
											<a href="{{ get_permalink($service->ID) }}" target="_blank" class="card service">
												@if(isset($service_icon))
													<img src="{{ $service_icon['url'] }}" width="70" alt="{{ $service->post_title }}">
												@else
													<img src="@asset('images/logo.svg')" width="100" alt="{{ $service->post_title }}">
												@endif
												<h5>{{ $service->post_title }}</h5>
											</a>
										</div>
									@endforeach
								</div>
								<div class="grid-x grid-margin-x mt3 show-for-small-only">
						    		<div class="cell">
										<div class="services standard-swiper swiper">
							           		<div class="swiper-wrapper">
							           			@foreach($services as $service)
							           				@php
														$service_icon = App\getServiceIcon($service);
													@endphp
													<div class="swiper-slide">
														<a href="{{ get_permalink($service->ID) }}" target="_blank" class="card service mb4">
															@if(isset($service_icon))
																<img src="{{ $service_icon['url'] }}" width="70" alt="{{ $service->post_title }}">
															@else
																<img src="@asset('images/logo.svg')" width="100" alt="{{ $service->post_title }}">
															@endif
															<h5>{{ $service->post_title }}</h5>
														</a>
													</div>
												@endforeach
							            	</div>
							            </div>
						            </div>
						        </div>
							@endif
						@endif

						<div class="card talk-to-us">
							<h3>Talk to a member of the DIS team on <br><a href="tel:01274 869099">01274 869099</a> to discuss your options</h3>
						</div>

						<hr class="mt4 mb4">

						@php
							$records = getSupportCases();
						@endphp

						<h2>Open Tickets</h2>
						@if($records)
							@foreach($records as $record)
								@if($record)
									@php $count=0; @endphp
									<div class="table-scroll mt3">
										<table>
											<thead>
												<tr>
													<td>No.</td>
													<td>Subject</td>
													<td>Date</td>
													<td>Case Type</td>
													<td>Status</td>
													<td>View</td>
												</tr>
											</thead>
											<tbody>
												@foreach($record as $case)
													@if($case->status->name != 'Closed')
														<tr>
															<td>{{ $case->caseNumber }}</td>
															<td>{{ $case->title }}</td>
															<td>{{ date("d/m/Y", strtotime($case->createdDate)); }}</td>
															@if($case->category)
																<td>{{ $case->category->name }}</td>
															@else
																<td>---</td>
															@endif
															@if($case->status)
																<td class="{{ sanitize_title($case->status->name); }}">{{ $case->status->name }}</td>
															@endif
															<td><a href="{{ home_url('/portal/ticket?id='.$case->internalId.'&case='.$case->caseNumber) }}">View Ticket</a></td>
														</tr>
														@php $count++; @endphp
													@endif
												@endforeach

												@if($count == 0)
													<tr>
														<td colspan="6">No open tickets available</td>
													</tr>
												@endif
											</tbody>
										</table>
									</div>
								@else
									<div class="table-scroll mt3">
										<table>
											<thead>
												<tr>
													<td>Service</td>
													<td>Created</td>
													<td>Due Date</td>
													<td>Status</td>
													<td>Amount</td>
												</tr>
											</thead>
											<tbody><tr><td colspan="6">No open tickets available</td></tr></tbody>
										</table>
									</div>
								@endif
							@endforeach
						@endif

						@php
							$member_level = SwpmMemberUtils::get_logged_in_members_level();
						@endphp

						@if($member_level == 2 || current_user_can('manage_options'))
							<hr class="mt4 mb4">
							<h2><?= date('F'); ?> Invoices</h2>

							@php
								$records = getMonthsPayments();
								$now = time();
							@endphp

							@if($records->record)
								@foreach($records as $record_list)
									@php
										$month_total = 0;
									@endphp
									<div class="table-scroll payments mt3">
										<table>
											<thead>
												<tr>
													<td>No.</td>
													<td>Title</td>
													<td>Invoice Date</td>
													<td>Due Date</td>
													<td>Status</td>
													<td>Amount</td>
												</tr>
											</thead>
											<tbody>
												@foreach($record_list as $invoice)

													@foreach($invoice->customFieldList->customField as $field)
														@if($field->scriptId == 'custbody_dis_ja_sale_title')
															@php $title = $field->value; @endphp
														@endif
													@endforeach

													@php
														if($invoice->status == 'Open') {
															if(strtotime($invoice->dueDate) < $now) {
																$pay_status = 'closed';
															} else {
																$pay_status = 'in-progress';
															}
														}
													@endphp

													<tr>
														<td>#{{ $invoice->tranId }}</td>
														@if($title)
															<td width="220">{{ $title }}</td>
														@endif
														<td>{{ date("d/m/Y", strtotime($invoice->tranDate)); }}</td>
														<td @if(isset($pay_status)) class="{{ $pay_status }}" @endif>{{ date("d/m/Y", strtotime($invoice->dueDate)); }}</td>
														<td>{{ $invoice->status }}</td>
														<td>£{{ number_format((float)$invoice->total, 2, '.', '') }}</td>
													</tr>

													@php
														$month_total = $month_total + $invoice->total;
													@endphp
												@endforeach

												<tr class="month-total">
													<td colspan="5"><h4>Total</h4></td>
													<td>£{{ number_format((float)$month_total, 2, '.', '') }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								@endforeach
							@else
								<div class="table-scroll mt3">
									<table>
										<thead>
											<tr>
												<td>No.</td>
												<td>Title</td>
												<td>Invoice Date</td>
												<td>Due Date</td>
												<td>Status</td>
												<td>Amount</td>
											</tr>
										</thead>
										<tbody><tr><td colspan="6">No invoices available.</td></tr></tbody>
									</table>
								</div>
							@endif
						@endif
					</div>
				</div>
			</div>
		</section>
	@endif

	@include('partials.flexible-content')

  @endwhile
@endsection
