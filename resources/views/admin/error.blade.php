<pre style='position: fixed; z-index: 9999999; bottom: 1rem; right: 1rem; background: #eee; padding: 1rem; border-radius: 5px; box-shadow: 0 0 5px rgb(0 0 0 / 10%); border-left: 5px solid #ff5722; line-height: 1.8; max-width: 80%; overflow: scroll;'>
  <div style="position: absolute; top: 0; right: 0; font-size: 2rem; cursor: pointer; padding: 5px; line-height: 1;" onclick="return this.parentNode.remove();">&times;</div>
  <strong>{{ $error_label }}</strong> {{ $message }}
  File: {{ $file }}
  Line: {{ $line }}
</pre>