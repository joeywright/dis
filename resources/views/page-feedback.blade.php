@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

	@include('partials.hero')

	@if( '' !== get_post()->post_content )
		<section data-viewport="detect" data-animate="fade" data-anchor="Introduction">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
					<div class="large-8 medium-10 small-12 cell">
						@if($fields['intro_text'])
							{!! $fields['intro_text'] !!}
						@endif
					</div>
				</div>
				<div class="grid-x grid-margin-x mt4">
					<div class="large-6 large-offset-4 medium-8 small-12 cell">
						@php the_content(); @endphp
					</div>
				</div>
			</div>
		</section>
	@endif

	<section data-viewport="detect" data-animate="fade" data-anchor="Get In Touch">
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="large-7 medium-6 small-12 cell">
					{!! gravity_form( 3, false, false, false, '', true ); !!}
				</div>
				<div class="large-4 large-offset-1 medium-6 small-12 cell">
					<div class="contact-info">
						@if($contact['email'])
							<p class="email"><a href="mailto:{{ $contact['email'] }}">{{ $contact['email'] }}</a></p>
						@endif
						@if($contact['phone'])
							<p class="phone"><a href="tel:{{ $contact['phone'] }}">{{ $contact['phone'] }}</a></p>
						@endif
						@if($contact['address'])
							<p class="address">{!! $contact['address'] !!}</p>
						@endif
					</div>
				</div>
			</div>
		</div>
	</section>

	@include('partials.flexible-content')

  @endwhile
@endsection
