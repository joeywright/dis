@if($data['image'])
    @if($data['image_only'])
	    <div class="image-block @if($data['background_colour'])panel {{ $data['background_colour'] }} @endif" data-viewport="detect" data-animate="fade" @if($data['anchor']) data-anchor="{{ $data['anchor'] }}" @endif>
	    	<div class="grid-container">
	    		<div class="grid-x grid-margin-x">
	    			<div class="large-12 cell">
						@if($data['mobile_image'] && $data['image'])
			    			<img src="{{ $data['image']['url'] }}"
								srcset="{{ $data['mobile_image']['url'] }} 650w,
								{{ $data['image']['url'] }} 1024w"
								sizes="(min-width: 640px) 2000px, (min-width: 640px) 600px, 100vw"
								alt="{{ $data['image']['alt'] }}" style="width: 100%;">
						@else
							<img src="{{ $data['image']['url'] }}" alt="{{ $data['image']['alt'] }}" style="width: 100%;">
						@endif
					</div>
				</div>
			</div>
		</div>
    @else
    	<div class="image-text-block" style="background-image: url('{{ $data['image']['url'] }}');" data-viewport="detect" data-animate="fade">
    		@if($data['text'])
    			<div class="grid-container">
    				<div class="grid-x grid-margin-x @if($data['vertical_position'] == 'middle') align-middle @elseif($data['vertical_position'] == 'bottom') align-bottom @endif @if($data['horizontal_position'] == 'center') align-center @elseif($data['horizontal_position'] == 'right') align-right @endif">
    					<div class="large-8 medium-10 small-12 cell">
    						{!! $data['text'] !!}
    					</div>
    				</div>
    			</div>
    		@endif
    	</div>
    @endif
@endif