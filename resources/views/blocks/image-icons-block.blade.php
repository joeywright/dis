<section class="image-icons-block @if($data['background_colour']) {{ $data['background_colour'] }} panel @endif" data-viewport="detect" data-animate="fade" @if($data['anchor']) data-anchor="{{ $data['anchor'] }}" @endif>
	<div class="grid-container">
		@if($data['intro_text'])
			<div class="grid-x grid-margin-x align-center mb4">
				<div class="large-8 medium-10 small-12 text-center cell">
			    	{!! $data['intro_text'] !!}
		        </div>
	    	</div>
		@endif

		<div class="grid-x grid-margin-x">
			@if($data['image'])
				<div class="large-5 medium-6 small-12 cell">
			    	<img src="{!! $data['image']['url'] !!}">
				</div>
			@endif
			<div class="large-6 large-offset-1 medium-6 small-12 cell">
				@if($data['icon_text'])
					<div class="intro">{!! $data['icon_text'] !!}</div>
				@endif
				@if($data['icons'])
					<div class="grid-x grid-margin-x hide-for-small-only small-up-1 medium-up-2 large-up-2 @if($data['icon_text']) mt3 @endif">
						@foreach ($data['icons'] as $key=>$icon)
							<div class="cell mb4">
								<div class="icon @if($data['align_icons']){{ $data['align_icons'] }}@endif">
									@if($icon['image'])
								  		<img src="{{ $icon['image']['url'] }}" alt="{{ $icon['image']['alt'] }}" @if($data['icon_size']) width="{{ $data['icon_size'] }}" @else width="60" @endif>
							  		@endif

							  		@if($icon['text'])
										<div class="content">{!! $icon['text'] !!}</div>
							  		@endif
								</div>
							</div>
						@endforeach
					</div>

			    	<div class="grid-x grid-margin-x show-for-small-only mt3">
			    		<div class="cell">
							<div class="standard-swiper swiper">
				           		<div class="swiper-wrapper">
				           			@foreach ($data['icons'] as $key=>$icon)
										<div class="swiper-slide">
											<div class="icon">
												@if($icon['image'])
											  		<img src="{{ $icon['image']['url'] }}" alt="{{ $icon['image']['alt'] }}" @if($data['icon_size']) width="{{ $data['icon_size'] }}" @else width="60" @endif>
										  		@endif

										  		@if($icon['text'])
													<div class="content">{!! $icon['text'] !!}</div>
										  		@endif
											</div>
										</div>
									@endforeach
				            	</div>
				            	<div class="nav">
                                    <div class="prev"><svg width="20" height="36" viewBox="0 0 20 36" xmlns="http://www.w3.org/2000/svg"><path d="M1 1l17 17L1 35" stroke="#00B1E0" stroke-width="2" fill="none" fill-rule="evenodd"/></svg></div>
                                   	<div class="swiper-pagination"></div>
                                    <div class="next"><svg width="20" height="36" viewBox="0 0 20 36" xmlns="http://www.w3.org/2000/svg"><path d="M1 1l17 17L1 35" stroke="#00B1E0" stroke-width="2" fill="none" fill-rule="evenodd"/></svg></div>
                                </div>
				            </div>
			            </div>
			        </div>
				@endif
			</div>
		</div>
	</div>
</section>
