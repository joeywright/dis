<section class="text-image-slider @if($data['background_colour'])panel {{ $data['background_colour'] }} @endif" @if($data['anchor']) data-anchor="{{ $data['anchor'] }}" @endif>
    <div class="grid-container">
	    <div class="grid-x grid-margin-x align-middle">
			<div class="standard-swiper fluid swiper">
			   	<div class="swiper-wrapper">
					@foreach ($data['slider'] as $slide)
						<div class="swiper-slide">
							<div class="grid-container">
								<div class="grid-x grid-margin-x align-middle">
									@if($slide['image'])
										<div class="medium-6 medium-order-2 small-12 cell">
										  	<img src="{{ $slide['image']['url'] }}" alt="{{ $slide['image']['alt'] }}" class="slide-img">
										</div>
									@endif
									@if($slide['text'])
										<div class="medium-6 medium-order-1 small-12 cell">
											{!! $slide['text'] !!}

											<div class="nav">
												<div class="prev"><img src="@asset('images/arrow-orange.svg')" width="25"></div>
											    <div class="swiper-pagination"></div>
												<div class="next"><img src="@asset('images/arrow-orange.svg')" width="25"></div>
											</div>
										</div>
									@endif
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</section>
