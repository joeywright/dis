<section class="icon-block @if($data['background_colour']) {{ $data['background_colour'] }} panel @endif" data-viewport="detect" data-animate="fade" @if($data['anchor']) data-anchor="{{ $data['anchor'] }}" @endif>
	<div class="grid-container">
		<div class="grid-x grid-margin-x mb4">
			@if($data['section_text'])
				<div class="large-6 medium-6 small-12 cell">
			    	{!! $data['section_text'] !!}
		        </div>
			@endif

		@if($data['layout'] == 'large')
			</div>
			<div class="grid-x grid-margin-x align-right">
		@endif

			@if($data['icons'])
				<div class="large-8 medium-10 small-12 cell">
					@if($data['columns'] == 2)
						<div class="grid-x grid-margin-x hide-for-small-only small-up-1 medium-up-2 large-up-2">
					@elseif($data['columns'] == 3)
						<div class="grid-x grid-margin-x hide-for-small-only small-up-1 medium-up-2 large-up-3">
					@elseif($data['columns'] == 4)
						<div class="grid-x grid-margin-x hide-for-small-only small-up-1 medium-up-2 large-up-4">
					@elseif($data['columns'] == 5)
						<div class="grid-x grid-margin-x hide-for-small-only small-up-1 medium-up-2 large-up-5">
					@elseif($data['columns'] == 6)
						<div class="grid-x grid-margin-x hide-for-small-only small-up-1 medium-up-2 large-up-6">
					@else
						<div class="grid-x grid-margin-x hide-for-small-only small-up-1 medium-up-2 large-up-2">
					@endif

						@foreach ($data['icons'] as $key=>$icon)
							<div class="cell mt3">
								<div class="icon">
									@if($icon['image'])
								  		<img src="{{ $icon['image']['url'] }}" alt="{{ $icon['image']['alt'] }}" class="mb3" @if($data['icon_size']) width="{{ $data['icon_size'] }}" @else width="60" @endif>
							  		@endif

							  		@if($icon['text'])
										<div class="content">{!! $icon['text'] !!}</div>
							  		@endif
								</div>
							</div>
						@endforeach
					</div>

			    	<div class="grid-x grid-margin-x show-for-small-only">
			    		<div class="cell">
							<div class="standard-swiper swiper">
				           		<div class="swiper-wrapper">
				           			@foreach ($data['icons'] as $key=>$icon)
										<div class="swiper-slide">
											<div class="icon text-center">
												@if($icon['image'])
											  		<img src="{{ $icon['image']['url'] }}" alt="{{ $icon['image']['alt'] }}" class="mb2"@if($data['icon_size']) width="{{ $data['icon_size'] }}" @else width="60" @endif >
										  		@endif

										  		@if($icon['text'])
													<div class="content">{!! $icon['text'] !!}</div>
										  		@endif
											</div>
										</div>
									@endforeach
				            	</div>
				            	<div class="nav">
		                            <div class="prev"><svg width="20" height="36" viewBox="0 0 20 36" xmlns="http://www.w3.org/2000/svg"><path d="M1 1l17 17L1 35" stroke="#00B1E0" stroke-width="2" fill="none" fill-rule="evenodd"/></svg></div>
		                            <div class="swiper-pagination"></div>
		                            <div class="next"><svg width="20" height="36" viewBox="0 0 20 36" xmlns="http://www.w3.org/2000/svg"><path d="M1 1l17 17L1 35" stroke="#00B1E0" stroke-width="2" fill="none" fill-rule="evenodd"/></svg></div>
		                        </div>
				            </div>
			            </div>
			        </div>
			    </div>
			@endif
		</div>
	</div>
</section>
