<section class="two-col-layout @if($data['background_colour'])panel border {{ $data['background_colour'] }} @else @if($data['anchor']) panel @endif @endif" @if($data['anchor']) data-anchor="{{ $data['anchor'] }}" @endif data-viewport="detect" data-animate="fade">

	@if($data['span_image'] == false)<div class="grid-container">@endif
		<div class="grid-x grid-margin-x align-middle">
			@foreach ($data['left_column'] as $column)
			 	@if($column['acf_fc_layout'] == 'text_block')
	        		<div class="large-5 medium-6 small-12 medium-order-1 small-order-2 @if($data['span_image'] == true) half-grid right @endif cell">
	    				{!! $column['text'] !!}
	    			</div>
	        	@elseif($column['acf_fc_layout'] == 'image_block')
		        	<div class="large-6 medium-6 small-12 cell">
						<img src="{{ $column['image']['url'] }}" alt="{{ $column['image']['alt'] }}" class="block-img">
	    			</div>
				@endif
			@endforeach

			@foreach ($data['right_column'] as $column)
			 	@if($column['acf_fc_layout'] == 'text_block')
	        		<div class="large-5 large-offset-1 medium-6 small-12 @if($data['span_image'] == true) half-grid @endif cell">
	    				{!! $column['text'] !!}
	    			</div>
	        	@elseif($column['acf_fc_layout'] == 'image_block')
		        	<div class="large-6 large-offset-1 medium-6 small-12 medium-order-2 small-order-1 cell">
						<img src="{{ $column['image']['url'] }}" alt="{{ $column['image']['alt'] }}" class="block-img">
	    			</div>
				@endif
			@endforeach
		</div>
	@if($data['span_image'] == false)</div>@endif
</section>