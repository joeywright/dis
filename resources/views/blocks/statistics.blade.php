<section class="@if($data['background_colour']) {{ $data['background_colour'] }} panel @endif" data-viewport="detect" data-animate="fade" @if($data['anchor']) data-anchor="{{ $data['anchor'] }}" @endif>
	<div class="grid-container">
		@if($data['statistic_text'])
			<div class="grid-x grid-margin-x mb4">
				<div class="large-8 medium-10 small-12  cell">
			    	{!! $data['statistic_text'] !!}
		        </div>
	    	</div>
		@endif
		<div class="statistics grid-x grid-margin-x">
			<div class="large-4 medium-6 small-6 cell">
				<hr class="medium-gray">
				@if($data['statistic_1'])
					<h2>{!! $data['statistic_1']!!}</h2>
				@endif
				<hr class="medium-gray">
				@if($data['statistic_text_1'])
					{!! $data['statistic_text_1']!!}
				@endif
			</div>
			<div class="large-4 medium-6 small-6 cell">
				<hr class="medium-gray">
				@if($data['statistic_2'])
					<h2>{!! $data['statistic_2']!!}</h2>
				@endif
				<hr class="medium-gray">
				@if($data['statistic_text_2'])
					{!! $data['statistic_text_2']!!}
				@endif
			</div>
			<div class="large-4 medium-6 small-12 cell">
				<hr class="medium-gray">
				@if($data['statistic_3'])
					<h2>{!! $data['statistic_3']!!}</h2>
				@endif
				<hr class="medium-gray">
				@if($data['statistic_text_3'])
					{!! $data['statistic_text_3']!!}
				@endif
			</div>
		</div>
	</div>
</section>
