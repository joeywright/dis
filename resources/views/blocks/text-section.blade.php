<section class="@if($data['background_colour']) panel {{ $data['background_colour'] }} @endif " @if($data['anchor'])id="{{ $data['anchor'] }}" @endif data-viewport="detect" data-animate="fade" @if($data['anchor']) data-anchor="{{ $data['anchor'] }}" @endif>
		<div class="grid-container">
			 @if($data['title'])
				<div class="grid-x grid-margin-x mb4">
		        	<div class="large-8 medium-10 small-12 cell">
		        		@if($data['subheading'])
						    <h5>{!! $data['subheading'] !!}</h5>
					    @endif
			           <h2 class="h1">{!! $data['title'] !!}</h2>
			    	</div>
		    	</div>
		    @endif

		    @if($data['columns'] == 1)
				<div class="grid-x grid-margin-x">
			        @if($data['one_col_text_block']['text_1'])
			        	<div class="large-10 medium-12 small-12 cell">
				           {!! $data['one_col_text_block']['text_1'] !!}
				    	</div>
				    @endif
			    </div>
			@elseif($data['columns'] == 2)
				<div class="grid-x grid-margin-x two-col">
		        	@if($data['two_col_text_block']['text_1'])
		        		<div class="large-5 medium-6 small-12 cell">
				           {!! $data['two_col_text_block']['text_1'] !!}
				    	</div>
				    @endif
				    @if($data['two_col_text_block']['text_2'])
		        		<div class="large-6 large-offset-1 medium-6 small-12 cell">
				           {!! $data['two_col_text_block']['text_2'] !!}
				    	</div>
				    @endif
			    </div>
			@elseif($data['columns'] == 3)
				<div class="grid-x grid-margin-x">
		        	@if($data['three_col_text_block']['text_1'])
		        		<div class="large-4 medium-6 small-12 cell">
				           {!! $data['three_col_text_block']['text_1'] !!}
				    	</div>
				    @endif
				    @if($data['three_col_text_block']['text_2'])
		        		<div class="large-4 medium-6 small-12 cell">
				           {!! $data['three_col_text_block']['text_2'] !!}
				    	</div>
				    @endif
				    @if($data['three_col_text_block']['text_3'])
		        		<div class="large-4 medium-6 small-12 cell">
				           {!! $data['three_col_text_block']['text_3'] !!}
				    	</div>
				    @endif
				</div>
			@endif
	</div>
</section>