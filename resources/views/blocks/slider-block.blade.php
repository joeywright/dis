@if($data['images'])
<hr>
<section @if($data['anchor'])id="{{ $data['anchor'] }}" @endif>
	@if($data['intro_text'])
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="large-8 medium-10 small-12 cell">
					{!! $data['intro_text'] !!}
				</div>
			</div>
		</div>
	@endif
	<div class="standard-swiper fluid mt4 swiper">
	   	<div class="swiper-wrapper">
			@foreach ($data['images'] as $image)
				<div class="swiper-slide">
					@if($image)
						<div class="grid-container">
							<div class="grid-x grid-margin-x">
								<div class="small-12 cell">
			  						<img src="{{ $image['url'] }}" alt="{{ $image['alt'] }}" class="main-image">

								    <div class="nav">
										<div class="prev"><img src="@asset('images/arrow-orange.svg')" width="30"></div>
										<div class="next"><img src="@asset('images/arrow-orange.svg')" width="30"></div>

										<div class="swiper-pagination hide-for-small-only"></div>
									</div>
			  					</div>
			  				</div>
			  			</div>

			  		@endif
				</div>
			@endforeach
		</div>
	</div>
</section>
@endif
