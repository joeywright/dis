<section class="accordions @if($data['background_colour'])panel {{ $data['background_colour'] }} @endif" @if($data['anchor']) data-anchor="{{ $data['anchor'] }}" @endif>
	<div class="grid-container">
		@if($data['description'])
		<div class="grid-x grid-margin-x mb4">
			<div class="large-8 medium-6 small-12 cell" data-animate="panel">
				{!! $data['description'] !!}
	        </div>
    	</div>
    	@endif

		@if($data['accordions'])
			<div class="grid-x grid-margin-x" data-animate="fade">
				<div class="large-8 medium-8 small-12 cell">
					<div id="accordion">
						@foreach($data['accordions'] as $key=>$accordion)
							<div class="accordion">
								@if($accordion['title'])
									<div class="accordion-title">
							  			<p>{{ $accordion['title'] }}</p>
							  		</div>
							  	@endif

							  	@if($accordion['description'])
									<div class="accordion-content">
							  			{!! $accordion['description'] !!}
							  		</div>
							  	@endif
							</div>
						@endforeach
					</div>
				</div>
				<div class="large-3 large-offset-1 medium-4 small-12 cell">
					<div class="boxed">
						<h3>Contact Us</h3>
						<p>We are always happy to talk about any legal concern you may have regarding criminal law matters.</p>
						<a href="<?= site_url(); ?>/contact" class="button mt3">Get In Touch</a>
					</div>
				</div>
			</div>
		@endif
	</div>
</section>