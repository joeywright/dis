@if($data['video'])
	<section class="secondary gradient panel split" data-viewport="detect" data-animate="fade" @if($data['anchor']) data-anchor="{{ $data['anchor'] }}" @endif>
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="small-12 cell">
					<div class="video-block" style="background-image: url('{{ $data['video_cover_image']['url'] }}');" data-viewport="detect" data-animate="fade">
						<a data-open="video-block"><img src="@asset('images/play.svg')" width="80"></a>

						<div class="modal video" id="video-block">
							<div class="close"></div>
							<div class="responsive-embed widescreen mb0">{!! $data['video'] !!}</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endif