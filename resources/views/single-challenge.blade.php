@extends('layouts.app')

@php
	$post = get_post();
	$terms = get_the_terms( $post->ID, 'challenge-categories' );
@endphp

@section('content')
   @while(have_posts()) @php the_post() @endphp

	<div class="hero" data-viewport="detect" data-animate="fade">
		<div class="grid-container">
			<div class="grid-x grid-margin-x align-middle">
				<div class="large-12 cell">
					<h5>Challenges</h5>
					<h1>{!! $title !!}</h1>
				</div>
			</div>
		</div>
		@if($featured_image)
			<div class="split bottom">
				<div class="grid-container">
					<div class="grid-x grid-margin-x align-middle">
						<div class="large-12 cell">
							<img src="{{ $featured_image }}">
						</div>
					</div>
				</div>
			</div>
		@endif
	</div>

	@if($terms)
		<div class="terms" data-viewport="detect" data-animate="fade">
			<div class="grid-container">
				<div class="grid-x grid-margin-x align-middle">
					<div class="large-12 cell">
						<ul class="menu">
							@foreach($terms as $term)
								@php
									$icon  = get_field('icon', $term->taxonomy . '_' . $term->term_id);
								@endphp
								<li class="mr2">
									@if($icon)
										<img src="{{ $icon['url'] }}" class="mr1" width="22">
									@endif
									<h5><a style="padding: 0;" href="{{ get_term_link( $term ) }}">{{ $term->name }}</a></h5>
								</li>
							@endforeach
						</ul>
					</div>
				</div>
			</div>
		</div>
		<hr class="m0">
	@endif

	@include('partials.flexible-content')

	<section data-viewport="detect" data-animate="fade" data-anchor="Let's DIScuss">
		<div class="grid-container">
			<div class="grid-x grid-margin-x align-center">
				<div class="large-8 medium-10 small-12 text-center cell">
					@if($global['cta_text'])
						{!! $global['cta_text'] !!}
					@endif
				</div>
			</div>
			<div class="grid-x grid-margin-x align-center mt4">
				<div class="large-8 medium-10 small-12 cell">
					{!! gravity_form( 1, false, false, false, '', true ); !!}
				</div>
			</div>
		</div>
	</section>

  	@endwhile

@endsection
