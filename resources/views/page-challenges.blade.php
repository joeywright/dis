@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

	@include('partials.hero')

	@if( '' !== get_post()->post_content )
		<section data-viewport="detect" data-animate="fade" data-anchor="Introduction">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
					<div class="large-6 medium-8 small-12 cell">
						@if($fields['intro_text'])
							{!! $fields['intro_text'] !!}
						@endif
					</div>
				</div>
				<div class="grid-x grid-margin-x mt4">
					<div class="large-6 large-offset-4 medium-8 small-12 cell">
						@php the_content(); @endphp
					</div>
				</div>
			</div>
		</section>
	@endif

	<section class="all-challenges panel secondary"  data-viewport="detect" data-animate="fade" data-anchor="Case Studies">
		<div class="grid-container">
			@if($challenges)
				<div class="grid-x grid-margin-x small-up-1 medium-up-2 large-up-2">
					@foreach($challenges->posts as $challenge)
						@php
							$terms = get_the_terms( $challenge->ID, 'challenge-categories' );
							$terms_string = join('<span style="color:#fff"> |</span> ', wp_list_pluck($terms, 'name'));
						@endphp
						<div class="cell mb4">
							<div class="post">
				  				@if(wp_get_attachment_url( get_post_thumbnail_id($challenge->ID)))
				  					<div class="image mb3" style="background-image: url('@php echo wp_get_attachment_url( get_post_thumbnail_id($challenge->ID)); @endphp');">
				  					</div>
								@else
									<div class="image mb3 placeholder" style="background-image: url('@asset('images/logo.svg')');"></div>
								@endif

								<h5>
									@foreach($terms as $term)
										<a style="padding: 0;" href="{{ get_term_link( $term ) }}">{{ $term->name }}</a><span style="color:#fff"> |</span>
									@endforeach
								</h5>
					  			<h3>{!! $challenge->post_title !!}</h3>
					  			<p>@php echo wp_trim_words($challenge->post_content, 20, '...'); @endphp</p>
								<a href="@php echo get_permalink( $challenge->ID) @endphp" class="button">View Challenge</a>
							</div>
				       	</div>
					@endforeach
				</div>
			@endif
		</div>
	</section>

	@include('partials.flexible-content')

	@include('partials.trusted-by-large')

  @endwhile
@endsection
