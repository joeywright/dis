{{--
  Template Name: Portal – Log A Ticket
--}}
@extends('layouts.portal')

@section('content')
  @while(have_posts()) @php the_post() @endphp

	@include('partials.hero-portal')

	<section data-viewport="detect" data-animate="fade" data-anchor="Introduction">
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="large-3 medium-4 small-12 cell">
					@include('partials.sidebar-portal')
				</div>
				<div class="large-8 large-offset-1 medium-8 small-12 cell">
					@php
						the_content();

						$get_contact = getCurrentCompany();
					@endphp

					<script type="text/javascript">
						jQuery(function($) {
							$(document).ready(() => {
								$(document).on('submit', '.submit-ticket', function(e) {
									e.preventDefault();
									var errors = 0;

					            	var title = $('input[name="title"]').val();
									var priority  = $('select[name="priority"]').val();
									var message = $('textarea[name="message"]').val();
									var status = $('input[name="status"]').val();
									var company = $('input[name="company"]').val();
									var contact_id = $('input[name="contact_id"]').val();

									$('.form-message').html('');

									if(title == '') {
										errors = 1;
										$('.form-message').append('<p>Please enter a title.</p>');
									}
									if(priority == '') {
										errors = 1;
										$('.form-message').append('<p>Please choose a priority.</p>');
									}
									if(message == '') {
										errors = 1;
										$('.form-message').append('<p>Please enter a message.</p>');
									}

									if(errors == 0) {
										$.ajax({
									        url: ajax_object.ajax_url,
									        type: 'GET',
									        data: {
									            'action': 'submit_case',
									            'title':  title,
									            'priority':  priority,
									            'message': message,
									            'status': status,
									            'company': company,
									            'contact_id': contact_id
									        },
									        beforeSend: function() {
									        	$('.loading').show();
									        },
									        success: function(data) {
									    		$('.form-message').html(data);
									        	$('.loading').hide();
											},
											error: function(MLHttpRequest, textStatus, errorThrown){
												console.log(errorThrown);
									        	$('.loading').hide();
											},
									    });
									}
							    });
				            });
			            });
					</script>

					<form class="submit-ticket mt3">
						<label>
							Title *
							<input type="text" name="title">
						</label>
						<label>
							Priority *
							<select name="priority">
								<option value="">---</option>
								<option value="1">High</option>
								<option value="2">Medium</option>
								<option value="3">Low</option>
							</select>
						</label>
						<label>
							Message *
							<textarea name="message" rows="4"></textarea>
						</label>

						<input type="hidden" value="1" name="status">
						<input type="hidden" value="{{ $get_contact->company->internalId }}" name="company">
						<input type="hidden" value="{{ $get_contact->internalId }}" name="contact_id">

						<div class="loading" style="display: none;"><img src="@asset('images/portal/loading.svg')" width="40"></div>
						<div class="form-message"></div>
						<button type="submit" class="button">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</section>

	@include('partials.flexible-content')

  @endwhile
@endsection
