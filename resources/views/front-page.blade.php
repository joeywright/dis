@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

	@if($fields['home_video'] || $fields['home_image'])
		<div class="home-hero">
			<div class="video-wrap">
				@if($fields['slider'])
					<div class="home-swiper swiper">
					   	<div class="swiper-wrapper">
				       		@foreach ($fields['slider'] as $slide)
								<div class="swiper-slide">
									@if($slide['home_text'])
										<div class="caption">{!! $slide['home_text'] !!}</div>
									@endif
					       		</div>
							@endforeach
						</div>
					</div>
				@endif

				@php
					if ( $fields['home_video'] ):
						if ( preg_match('/src="(.+?)"/', $fields['home_video'], $matches) ):
							// Video source URL
							$src = $matches[1];
							// Add option to hide controls, enable HD, and do autoplay -- depending on provider
							$params = array(
								'controls'    => 0,
								'hd'        => 1,
								'autoplay' => 1,
								'background' => 1,
							);

							$new_src = add_query_arg($params, $src);
							$fields['home_video'] = str_replace($src, $new_src, $fields['home_video']);

							// add extra attributes to iframe html
							$attributes = 'frameborder="0"';
							$fields['home_video'] = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $fields['home_video']);
						endif;

						echo '<div class="video">', $fields['home_video'], '</div>';
					endif;
				@endphp
		  	</div>

			@if($fields['trusted_by'])
				<div class="logo-grid">
					<div class="grid-container">
						<div class="grid-x small-up-4 medium-up-4 large-up-6">
							<div class="cell">
								<h5>Trusted By</h5>
							</div>
							@foreach($fields['trusted_by'] as $logo)
								<div class="cell">
									@if($logo['url'])
										<img src="{!! $logo['url'] !!}">
							  		@endif
								</div>
							@endforeach
						</div>
					</div>
				</div>
			@endif
		</div>
	@endif

	@if( '' !== get_post()->post_content )
		<section data-viewport="detect" data-animate="fade">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
					<div class="large-8 medium-10 small-12 cell">
						@if($fields['intro_text'])
							{!! $fields['intro_text'] !!}
						@endif
					</div>
				</div>
				<div class="grid-x grid-margin-x mt4">
					<div class="large-6 large-offset-4 medium-8 small-12 cell">
						@php the_content(); @endphp
					</div>
				</div>
			</div>
		</section>
	@endif

	@include('partials.flexible-content')

	@include('partials.cta')
  @endwhile
@endsection