{{--
  Template Name: Portal – Tickets
--}}
@extends('layouts.portal')

@section('content')
  @while(have_posts()) @php the_post() @endphp

	@include('partials.hero-portal')

	<section data-viewport="detect" data-animate="fade" data-anchor="Introduction">
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="large-3 medium-4 small-12 cell">
					@include('partials.sidebar-portal')
				</div>
				<div class="large-8 large-offset-1 medium-8 small-12 cell">
					@php
						$records = getSupportCases();
						$closedRecords = getClosedSupportCases();
					@endphp

					<h2>Open Tickets</h2>
					@if($records)
						@foreach($records as $record)
							@if($record)
								@php $count=0; @endphp
								<div class="table-scroll mt3">
									<table>
										<thead>
											<tr>
												<td>No.</td>
												<td>Subject</td>
												<td>Date</td>
												<td>Case Type</td>
												<td>Status</td>
												<td>View</td>
											</tr>
										</thead>
										<tbody>
											@foreach($record as $case)
												@if($case->status->name != 'Closed')
													<tr>
														<td>{{ $case->caseNumber }}</td>
														<td>{{ $case->title }}</td>
														<td>{{ date("d/m/Y", strtotime($case->createdDate)); }}</td>
														@if($case->category)
															<td>{{ $case->category->name }}</td>
														@else
															<td>---</td>
														@endif
														@if($case->status)
															<td class="{{ sanitize_title($case->status->name); }}">{{ $case->status->name }}</td>
														@endif
														<td><a href="{{ home_url('/portal/ticket?id='.$case->internalId.'&case='.$case->caseNumber) }}">View Ticket</a></td>
													</tr>

													@php $count++; @endphp
												@endif
											@endforeach

											@if($count == 0)
												<tr>
													<td colspan="6">No open tickets available</td>
												</tr>
											@endif
										</tbody>
									</table>
								</div>
							@else
								<p>No support cases available</p>
							@endif
						@endforeach
					@endif

					<h2 class="mt4">Closed Tickets</h2>
					<p>Within 60 days.</p>
					@if($closedRecords)
						@foreach($closedRecords as $record)
							@if($record)
								<div class="table-scroll mt3">
									<table>
										<thead>
											<tr>
												<td>No.</td>
												<td>Subject</td>
												<td>Date</td>
												<td>Case Type</td>
												<td>Status</td>
												<td>View</td>
											</tr>
										</thead>
										<tbody>
											@foreach($record as $case)
												<tr>
													<td>{{ $case->caseNumber }}</td>
													<td>{{ $case->title }}</td>
													<td>{{ date("d/m/Y", strtotime($case->createdDate)); }}</td>
													@if($case->category)
														<td>{{ $case->category->name }}</td>
													@else
														<td>---</td>
													@endif
													@if($case->status)
														<td class="{{ sanitize_title($case->status->name); }}">{{ $case->status->name }}</td>
													@endif
													<td><a href="{{ home_url('/portal/ticket?id='.$case->internalId.'&case='.$case->caseNumber) }}">View Ticket</a></td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							@else
								<p>No support cases available</p>
							@endif
						@endforeach
					@endif
				</div>
			</div>
		</div>
	</section>

	@include('partials.flexible-content')

  @endwhile
@endsection
