{{--
  Template Name: Portal – Services
--}}
@extends('layouts.portal')

@section('content')
  @while(have_posts()) @php the_post() @endphp

	@include('partials.hero-portal')

		<section data-viewport="detect" data-animate="fade" data-anchor="Introduction">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
					<div class="large-3 medium-4 small-12 cell">
						@include('partials.sidebar-portal')
					</div>
					<div class="large-8 large-offset-1 medium-8 small-12 cell">
						@if( '' !== get_post()->post_content )
							@php the_content(); @endphp
						@endif

						@if(is_user_logged_in())
							@php
								$services = get_field('services_in_use', 'user_'. get_current_user_id());
								$upsells  = get_field('services_you_might_be_interested_in', 'user_'. get_current_user_id());
							@endphp

							@if($services)
								<h2 class="m0">Services You're Using</h2>
								<div class="grid-x grid-margin-x large-up-3 medium-up-2 small-up-1 mt3 hide-for-small-only">
									@foreach($services as $service)
										@php
											$service_icon = App\getServiceIcon($service);
										@endphp
										<div class="cell mb3">
											<a href="{{ get_permalink($service->ID) }}" target="_blank" class="card service">
												@if(isset($service_icon))
													<img src="{{ $service_icon['url'] }}" width="70" alt="{{ $service->post_title }}">
												@else
													<img src="@asset('images/logo.svg')" width="100" alt="{{ $service->post_title }}">
												@endif
												<h5>{{ $service->post_title }}</h5>
											</a>
										</div>
									@endforeach
								</div>
								<div class="grid-x grid-margin-x mt3 show-for-small-only">
						    		<div class="cell">
										<div class="services standard-swiper swiper">
							           		<div class="swiper-wrapper">
							           			@foreach($services as $service)
							           				@php
														$service_icon = App\getServiceIcon($service);
													@endphp
													<div class="swiper-slide">
														<a href="{{ get_permalink($service->ID) }}" target="_blank" class="card service mb4">
															@if(isset($service_icon))
																<img src="{{ $service_icon['url'] }}" width="70" alt="{{ $service->post_title }}">
															@else
																<img src="@asset('images/logo.svg')" width="100" alt="{{ $service->post_title }}">
															@endif
															<h5>{{ $service->post_title }}</h5>
														</a>
													</div>
												@endforeach
							            	</div>
							            </div>
						            </div>
						        </div>
							@endif

							@if($upsells)
								<h3 class="m0 mt3">Services We Think You'd Be Interested In</h3>
								<div class="grid-x grid-margin-x large-up-3 medium-up-2 small-up-1 mt3 hide-for-small-only">
									@foreach($upsells as $upsell)
										@php
											$upsell_icon = App\getServiceIcon($upsell);
										@endphp
										<div class="cell mb3">
											<a href="{{ get_permalink($upsell->ID) }}" target="_blank" class="card service">
												@if(isset($upsell_icon))
													<img src="{{ $upsell_icon['url'] }}" width="70" alt="{{ $upsell->post_title }}">
												@else
													<img src="@asset('images/logo.svg')" width="100" alt="{{ $upsell->post_title }}">
												@endif
												<h5>{{ $upsell->post_title }}</h5>
											</a>
										</div>
									@endforeach
								</div>
								<div class="grid-x grid-margin-x mt3 show-for-small-only">
						    		<div class="cell">
										<div class="services standard-swiper swiper">
							           		<div class="swiper-wrapper">
							           			@foreach($upsells as $upsell)
										           	@php
														$upsell_icon = App\getServiceIcon($upsell);
													@endphp
													<div class="swiper-slide">
														<a href="{{ get_permalink($upsell->ID) }}" target="_blank" class="card service mb4">
															@if(isset($upsell_icon))
																<img src="{{ $upsell_icon['url'] }}" width="70" alt="{{ $upsell->post_title }}">
															@else
																<img src="@asset('images/logo.svg')" width="100" alt="{{ $upsell->post_title }}">
															@endif
															<h5>{{ $upsell->post_title }}</h5>
														</a>
													</div>
												@endforeach
							            	</div>
							            </div>
						            </div>
						        </div>
							@endif
						@endif
					</div>
				</div>
			</div>
		</section>

	@include('partials.flexible-content')

  @endwhile
@endsection
