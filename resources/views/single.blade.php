@extends('layouts.app')

@section('content')
   @while(have_posts()) @php the_post() @endphp

	<div class="hero" data-viewport="detect" data-animate="fade">
		@if($featured_image)
			<div class="split bottom">
				<div class="grid-container">
					<div class="grid-x grid-margin-x align-middle">
						<div class="large-12 cell">
							<img src="{{ $featured_image }}">
						</div>
					</div>
				</div>
			</div>
		@endif
	</div>

	<div class="post-content">
	  	@if( '' !== get_post()->post_content )
			<section>
				<div class="grid-container">
					<div class="grid-x grid-margin-x">
						<div class="large-8 medium-10 small-12 cell">
							<h5><time class="updated" datetime="{{ get_post_time('c', true) }}">{{ get_the_date('d/m/Y') }}</time></h5>
				    		<h1 class="h2 mt3">{!! the_title() !!}</h1>
				    	</div>
				    </div>
					<div class="grid-x grid-margin-x align-right mt4">
						<div class="large-8 medium-10 small-12 cell">
							@php the_content(); @endphp
						</div>
					</div>
				</div>
			</section>
		@endif

		@include('partials.flexible-content')

	</div>

  	@endwhile

@endsection
