{{--
  Template Name: Portal – Payments
--}}
@extends('layouts.portal')

@section('content')
  @while(have_posts()) @php the_post() @endphp

	@include('partials.hero-portal')

	<section data-viewport="detect" data-animate="fade" data-anchor="Introduction">
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="large-3 medium-4 small-12 cell">
					@include('partials.sidebar-portal')
				</div>
				<div class="large-8 large-offset-1 medium-8 small-12 cell">
					@php
						$member_level = SwpmMemberUtils::get_logged_in_members_level();
					@endphp

					@if($member_level == 2 || current_user_can('manage_options'))
						<script type="text/javascript">
							jQuery(document).ready(function($) {
								//go to link on select change
								var current_year = new Date().getFullYear();

								$('[data-year="'+current_year+'"]').show();

								$('.year-select').on('change', function(e) {
									var val = $(this).val();
									if(val != '') {
										$('.year-wrap').hide();

										$('[data-year="'+val+'"]').fadeIn();
										$('.year-select').val(val);
									}
								});
							});
						</script>

						@php
							$records = getPayments();
							$years = array();
							$sortedRecords = array();
							$now = time();

							foreach($records as $record) {
								if($record) {
									foreach($record as $key => $invoice) {
										$date = $invoice->tranDate;

										//Sorted by year then month
										$year  = date('Y', strtotime($date));
										$month = date('F', strtotime($date));
										$years[] = $year;
										$sortedRecords[$year][$month][] = $invoice;
									}
								}
							}

							$years = array_unique($years);
						@endphp

						@if($sortedRecords)
							@foreach($sortedRecords as $key => $year)
								<div class="year-wrap" data-year="{{ $key }}" style="display: none;">
									@foreach($year as $key => $month)
										@php
											$month_total = 0;
										@endphp

										@if($loop->first)
											<div class="grid-x grid-margin-x">
												<div class="large-8 medium-12 small-12 cell mb3-s">
													<h2>{{ $key }}</h2>
												</div>
												<div class="large-4 medium-12 small-12 cell">
													<select class="year-select">
														<option value="">Filter by year</option>
														@foreach($years as $year)
															<option>{{ $year }}</option>
														@endforeach
													</select>
												</div>
											</div>
										@else
											<h2 class="mt3">{{ $key }}</h2>
										@endif

										<div class="table-scroll payments mt3">
											<table>
												<thead>
													<tr>
														<td>No.</td>
														<td>Title</td>
														<td>Invoice Date</td>
														<td>Due Date</td>
														<td>Status</td>
														<td>Amount</td>
													</tr>
												</thead>
												<tbody>
													@foreach($month as $invoice)
														@foreach($invoice->customFieldList->customField as $field)
															@if($field->scriptId == 'custbody_dis_ja_sale_title')
																@php $title = $field->value; @endphp
															@endif
														@endforeach

														@php
															if($invoice->status == 'Open') {
																if(strtotime($invoice->dueDate) < $now) {
																	$pay_status = 'closed';
																} else {
																	$pay_status = 'in-progress';
																}
															}
														@endphp

														<tr>
															<td>#{{ $invoice->tranId }}</td>
															@if($title)
																<td width="220">{{ $title }}</td>
															@endif
															<td>{{ date("d/m/Y", strtotime($invoice->tranDate)); }}</td>
															<td @if(isset($pay_status)) class="{{ $pay_status }}" @endif>{{ date("d/m/Y", strtotime($invoice->dueDate)); }}</td>
															<td>{{ $invoice->status }}</td>
															<td>£{{ number_format((float)$invoice->total, 2, '.', '') }}</td>
														</tr>

														@php
															$month_total = $month_total + $invoice->total;
														@endphp
													@endforeach

													<tr class="month-total">
														<td colspan="5"><h4>Total</h4></td>
														<td>£{{ number_format((float)$month_total, 2, '.', '') }}</td>
													</tr>
												</tbody>
											</table>
										</div>
									@endforeach
								</div>
							@endforeach
						@else
							<p>No invoices available.</p>
						@endif
					@else
						<p>Restricted area.</p>
					@endif
				</div>
			</div>
		</div>
	</section>

	@include('partials.flexible-content')

  @endwhile
@endsection
