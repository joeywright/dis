{{--
  Template Name: Portal – Downloads
--}}
@extends('layouts.portal')

@section('content')
  @while(have_posts()) @php the_post() @endphp

	@include('partials.hero-portal')

		<section data-viewport="detect" data-animate="fade" data-anchor="Introduction">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
					<div class="large-3 medium-4 small-12 cell">
						@include('partials.sidebar-portal')
					</div>
					<div class="large-8 large-offset-1 medium-8 small-12 cell">
						@if( '' !== get_post()->post_content )
							@php the_content(); @endphp
						@endif

						@php $downloads = get_field('documents', 'user_'. get_current_user_id()); @endphp
						@if($downloads)
							<div class="grid-x grid-margin-x large-up-3 medium-up-2 small-up-1 mt3">
								@foreach($downloads as $download)
									<div class="cell mb3">
										<a href="{{ get_permalink($download->ID) }}" class="card download" target="_blank">
											<h5>{{ $download->post_title }}</h5>
											<div class="link">Download</div>
										</a>
									</div>
								@endforeach
							</div>
						@endif
					</div>
				</div>
			</div>
		</section>

	@include('partials.flexible-content')

  @endwhile
@endsection
