@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

	@include('partials.hero')

	@if( '' !== get_post()->post_content )
		<section data-viewport="detect" data-animate="fade" data-anchor="Introduction">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
					<div class="large-8 medium-10 small-12 cell">
						@if($fields['intro_text'])
							{!! $fields['intro_text'] !!}
						@endif
					</div>
				</div>
			</div>
		</section>
	@endif

	<section class="contact-us secondary panel" style="padding-bottom: 0;" data-viewport="detect" data-animate="fade" data-anchor="Get In Touch">
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="large-5 medium-6 small-12 cell">
					<div class="contact-info">
						@if($contact['email'])
							<p class="email"><a href="mailto:{{ $contact['email'] }}">{{ $contact['email'] }}</a></p>
						@endif
						@if($contact['phone'])
							<p class="phone"><a href="tel:{{ $contact['phone'] }}">{{ $contact['phone'] }}</a></p>
						@endif
						@if($contact['address'])
							<p class="address">{!! $contact['address'] !!}</p>
						@endif
					</div>
				</div>
				<div class="large-6 large-offset-1 medium-6 small-12 cell">
					<div class="mb3">@php the_content(); @endphp</div>

					{!! gravity_form( 1, false, false, false, '', true ); !!}
				</div>
			</div>
			<div class="grid-x grid-margin-x">
				<div class="small-12 cell">
					<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyArilsG1h0CcrzYgXlqT9eU623NqoVOY2o&callback=initialize"async defer></script>

					<script type="text/javascript">
					    function initialize() {
					        var map;
					        var myLatlng = new google.maps.LatLng(53.730722626153785, -1.713250425519667);
					        var my_maptype_id = 'DIS';


					        var mapOptions = {
					            zoom: 15,
					            center: myLatlng,
					            scrollwheel: false,
					            styles: [{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#d3d3d3"}]},{"featureType":"transit","stylers":[{"color":"#808080"},{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#b3b3b3"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"weight":1.8}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"color":"#d7d7d7"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ebebeb"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#a7a7a7"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#efefef"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#696969"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#737373"}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#d6d6d6"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#dadada"}]}]
					        };

					        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

					        var styledMapOptions = {
					            name: 'DIS'
					        };

					        var image = {
					            url: '<?php bloginfo('stylesheet_directory'); ?>/public/images/marker.png',
					            size: new google.maps.Size(64, 64),
					        };

					        var marker = new google.maps.Marker({
					            position: myLatlng,
					            map: map,
					            title: 'DIS',
					            icon: image,
					        });

					        var customMapType = new google.maps.StyledMapType(styledMapOptions);
					        map.mapTypes.set(my_maptype_id, customMapType);
					        map.setCenter(myLatlng);
					    }
					</script>

					<div class="responsive-embed widescreen mb0 mt4" id="map-canvas"></div>
				</div>
			</div>
		</div>
	</section>

	@include('partials.flexible-content')

	@include('partials.trusted-by-large')

  @endwhile
@endsection
