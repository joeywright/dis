<div class="cell mb3" @php post_class() @endphp>
	<a href="{{ get_permalink() }}" class="post">
		@if(get_the_post_thumbnail_url())
			<div class="image" style="background-image: url(@php echo get_the_post_thumbnail_url(); @endphp)"></div>
		@else
			<div class="image placeholder"></div>
		@endif
		<div class="content mt3">
			<h3>{{ get_the_title() }}</h3>
			<hr class="small">
			<p>@php echo wp_trim_words(get_the_content(), 18, '...'); @endphp</p>
			<div class="text-link">Read More</div>
		</div>
	</a>
</div>
