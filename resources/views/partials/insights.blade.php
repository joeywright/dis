<section data-viewport="detect" data-animate="fade" data-anchor="Insights">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="large-6 medium-9 small-12 cell">
				<h5>THE LATEST INDUSTRY INSIGHTS</h5>
				<h1>Insights</h1>
			</div>
		</div>
		@if($blog_posts)
			<div class="grid-x grid-margin-x mt4">
				<div class="large-12 cell">
					<div class="insights card-swiper swiper">
						<div class="swiper-wrapper">
							@foreach($blog_posts->posts as $post)
								<div class="swiper-slide">
									<a href="@php echo get_permalink( $post->ID) @endphp" class="post card">
										@php
											$icon = get_field('icon_white', $post->ID);
										@endphp
						  				@if(wp_get_attachment_url( get_post_thumbnail_id($post->ID)))
						  					<div class="image" style="background-image: url('@php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID)); @endphp');">
						  						@if($icon)
						  							<div class="icon"><img width="60" src="{{ $icon['url'] }}"></div>
						  						@endif
						  					</div>
										@else
											<div class="image placeholder" style="background-image: url('@asset('images/logo.svg')');"></div>
										@endif
										<div class="content">
											<h6>{{ get_the_date('d/m/Y', $post->ID) }}</h6>
								  			<h3>{!! $post->post_title !!}</h3>
								  			<p>@php echo wp_trim_words($post->post_content, 20, '...'); @endphp</p>
											<div class="follow-link">Read Insight</div>
										</div>
									</a>
						       	</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		@endif
	</div>
</section>