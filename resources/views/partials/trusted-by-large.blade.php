@if($global['brands'])
	<section data-viewport="detect" data-animate="fade" data-anchor="Prominent Brands">
		<div class="grid-container">
			<div class="grid-x grid-margin-x align-bottom">
				<div class="large-8 medium-9 small-12 cell">
					<h5>DIS is</h5>
					<h1>Trusted By<br> Prominent Brands</h1>
				</div>
			</div>
			<div class="brands grid-x grid-margin-x small-up-2 medium-up-4 large-up-6 mt3">
				@foreach($global['brands'] as $partner)
					<div class="cell text-center mt3">
						@if($partner['url'])
							<div class="brand-box">
								<img src="{!! $partner['url'] !!}">
							</div>
				  		@endif
					</div>
				@endforeach
			</div>
		</div>
	</section>
@endif