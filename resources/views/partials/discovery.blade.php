<section class="statistics secondary panel" data-viewport="detect" data-animate="fade" data-anchor="Discovery">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="large-5 medium-6 small-12 cell">
				@if($global['discovery_text'])
					{!! $global['discovery_text'] !!}
				@endif
			</div>
			@if($global['discovery_boxes'])
				<div class="large-6 large-offset-1 medium-6 small-12 cell mt4">
					<div class="discovery-swiper swiper">
						<div class="swiper-wrapper">
							@foreach($global['discovery_boxes'] as $box)
								<div class="swiper-slide">
									<div class="discovery-box">
						  				@if($box['icon'])
											<img width="60" src="{!! $box['icon']['url'] !!}" class="mb3">
										@endif
										@if($box['text'])
											{!! $box['text'] !!}
										@endif
									</div>
						       	</div>
							@endforeach
						</div>
						<div class="nav">
                            <div class="prev"><svg width="47" height="23" viewBox="0 0 47 23" xmlns="http://www.w3.org/2000/svg"><g stroke="#fff" stroke-width="2" fill="none" fill-rule="evenodd"><path d="M0 11.501h29.124M25.012 1 44 11.501 25.012 22"/></g></svg></div>
                            <div class="swiper-pagination"></div>
                            <div class="next"><svg width="47" height="23" viewBox="0 0 47 23" xmlns="http://www.w3.org/2000/svg"><g stroke="#fff" stroke-width="2" fill="none" fill-rule="evenodd"><path d="M0 11.501h29.124M25.012 1 44 11.501 25.012 22"/></g></svg></div>
                        </div>
					</div>
				</div>
			@endif
		</div>
	</div>
</section>