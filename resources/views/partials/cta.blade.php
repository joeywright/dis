
<section class="cta panel secondary" data-viewport="detect" data-animate="fade" data-anchor="Let's DIScuss">
	<div class="grid-container">
		<div class="grid-x grid-margin-x align-center">
			<div class="large-8 medium-10 small-12 text-center cell">
				@if($global['cta_text'])
					{!! $global['cta_text'] !!}
				@endif
			</div>
		</div>
		<div class="grid-x grid-margin-x align-center mt4">
			<div class="large-8 medium-10 small-12 cell">
				{!! gravity_form( 1, false, false, false, '', true ); !!}
			</div>
		</div>
	</div>
</section>