
	<header>
		<div class="upper-nav show-for-small-only">
			<ul class="menu">
				@if($contact['email'])
					<li class="email"><a href="mailto:{{ $contact['email'] }}">{{ $contact['email'] }}</a></li>
				@endif
				@if($contact['phone'])
					<li class="phone"><a href="tel:{{ $contact['phone'] }}">{{ $contact['phone'] }}</a></li>
				@endif
			</ul>
		</div>
		<div class="grid-container">
			<div class="grid-x grid-margin-x align-middle">
				<div class="large-3 medium-3 small-6 cell">
					<a href="{{ home_url('/') }}" class="logo">
						<img src="@asset('images/logo-white.svg')" width="200" alt="DIS Logo">
					</a>
				</div>
				<div class="large-9 medium-6 small-6 cell">
					<div class="upper-nav">
						<div class="grid-container">
							<div class="grid-x grid-margin-x">
								<div class="cell">
									<ul class="menu show-for-large">
										@if($contact['email'])
											<li class="email"><a href="mailto:{{ $contact['email'] }}">{{ $contact['email'] }}</a></li>
										@endif
										@if($contact['phone'])
											<li class="phone"><a href="tel:{{ $contact['phone'] }}">{{ $contact['phone'] }}</a></li>
										@endif
						        		{!! wp_nav_menu(['container' => 'ul', 'items_wrap' => '%3$s', 'menu' => 'Upper Nav', 'menu_class' => 'vertical menu', 'walker' => new \App\Foundation_Walker()]) !!}
						        	</ul>
						        </div>
					        </div>
					    </div>
					</div>

					@if (has_nav_menu('primary_navigation'))
						<nav class="main-navigation show-for-large" role="navigation">
							<ul class="menu">
				        		{!! wp_nav_menu(['container' => 'ul', 'items_wrap' => '%3$s', 'theme_location' => 'primary_navigation', 'menu_class' => 'vertical menu', 'walker' => new \App\Foundation_Walker()]) !!}
				        	</ul>
						</nav>
			      	@endif

					<a href="#" class="show-for-small-only dot-toggle">
						<span></span>
						<span></span>
						<span></span>

						<span></span>
						<span></span>
						<span></span>

						<span></span>
						<span></span>
						<span></span>
					</a>
				</div>
			</div>
		</div>

		<div class="overlay-menu hide-for-large">
			<ul class="menu vertical">
				{!! wp_nav_menu(['container' => 'ul', 'items_wrap' => '%3$s', 'theme_location' => 'primary_navigation', 'menu_class' => 'vertical menu', 'walker' => new \App\Foundation_Walker()]) !!}
				<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1484"><a href="<?= site_url(); ?>/portal-login/">Login</a></li>
	    	</ul>
		</div>

		<div class="fixed-nav hide-for-small-only">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
					<div class="cell">
						<ul class="menu"></ul>
					</div>
				</div>
			</div>

			<div class="progress-wrapper"><progress></progress></div>
		</div>
	</header>

