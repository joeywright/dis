<section data-viewport="detect" data-animate="fade" data-anchor="What We Do">
	<div class="grid-container">
		<div class="grid-x grid-margin-x align-bottom">
			<div class="large-6 medium-9 small-12 cell">
				@if($global['what_we_do_text'])
					{!! $global['what_we_do_text'] !!}
				@endif
			</div>
		</div>
		@if($solution_categories)
			<div class="grid-x grid-margin-x small-up-1 medium-up-2 large-up-3 hide-for-small-only mt3">
				@foreach($solution_categories as $category)
					<div class="cell mt3">
						<a href="@php echo get_term_link( $category->term_id) @endphp" class="post" id="{{ $category->slug }}">
							@php
								$image = get_field('featured_image', $category->taxonomy . '_' . $category->term_id);
								$icon  = get_field('icon_white', $category->taxonomy . '_' . $category->term_id);
							@endphp
			  				@if($image)
			  					<div class="image mb3" style="background-image: url('{{ $image['url'] }}');">
			  						@if($icon)
			  							<div class="icon"><img src="{{ $icon['url'] }}"></div>
			  						@endif
			  					</div>
							@else
								<div class="image mb3 placeholder" style="background-image: url('@asset('images/logo.svg')');"></div>
							@endif

				  			<h3>{!! $category->name !!}</h3>
				  			<p class="secondary-text">@php echo wp_trim_words($category->description, 30, '...'); @endphp</p>
							<div class="button">Find Out More</div>
						</a>
			       	</div>
				@endforeach
			</div>
			<div class="grid-x grid-margin-x show-for-small-only mt4">
	    		<div class="cell">
					<div class="standard-swiper swiper">
		           		<div class="swiper-wrapper">
		           			@foreach($solution_categories as $category)
								<div class="swiper-slide">
									<a href="@php echo get_term_link( $category->term_id) @endphp" class="post" id="{{ $category->slug }}">
										@php
											$image = get_field('featured_image', $category->taxonomy . '_' . $category->term_id);
											$icon  = get_field('icon_white', $category->taxonomy . '_' . $category->term_id);
										@endphp
						  				@if($image)
						  					<div class="image mb3" style="background-image: url('{{ $image['url'] }}');">
						  						@if($icon)
						  							<div width="60" class="icon"><img src="{{ $icon['url'] }}"></div>
						  						@endif
						  					</div>
										@else
											<div class="image mb3 placeholder" style="background-image: url('@asset('images/logo.svg')');"></div>
										@endif

							  			<h3>{!! $category->name !!}</h3>
							  			<p class="secondary-text">@php echo wp_trim_words($category->description, 30, '...'); @endphp</p>
										<div class="button">Find Out More</div>
									</a>
								</div>
							@endforeach
		            	</div>
		            </div>
	            </div>
	        </div>
		@endif
	</div>
</section>