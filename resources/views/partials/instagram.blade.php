<section data-viewport="detect" data-animate="fade">
	<div class="grid-container">
		<div class="grid-x grid-margin-x align-center">
			<div class="large-8 medium-10 small-12 cell text-center">
				@if($global['instagram_text'])
					{!! $global['instagram_text'] !!}
				@endif
			</div>
		</div>
		<div class="grid-x grid-margin-x align-center mt3">
			<div class="small-12 cell text-center">
				{!! do_shortcode('[grace id="1"]'); !!}
			</div>
		</div>
	</div>
</section>