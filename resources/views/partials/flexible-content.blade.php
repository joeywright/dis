@if (array_key_exists('page_sections', $fields))
	@if($fields['page_sections'])
		@foreach ($fields['page_sections'] as $data)
		    @if($data['acf_fc_layout'] == 'icon_block' )

		    	@include('blocks.icons')

			@elseif( $data['acf_fc_layout'] == 'two_column_block' )

				@include('blocks.two-columns')

			@elseif( $data['acf_fc_layout'] == 'text_section' )

				@include('blocks.text-section')

			@elseif( $data['acf_fc_layout'] == 'image_block' )

				@include('blocks.fluid-image')

			@elseif( $data['acf_fc_layout'] == 'image_icons_block' )

				@include('blocks.image-icons-block')

			@elseif( $data['acf_fc_layout'] == 'slider_block' )

				@include('blocks.slider-block')

			@elseif( $data['acf_fc_layout'] == 'text_slider_block' )

				@include('blocks.text-slider-block')

			@elseif( $data['acf_fc_layout'] == 'video_block' )

				@include('blocks.video-block')

			@elseif( $data['acf_fc_layout'] == 'accordion_block' )

				@include('blocks.accordion-block')

			@elseif( $data['acf_fc_layout'] == 'statistics_block' )

				@include('blocks.statistics')

			@elseif( $data['acf_fc_layout'] == 'divider' )

				<hr>

			@elseif( $data['acf_fc_layout'] == 'global_blocks' )

				@if($data['blocks'] == 'testimonials')

					@include('partials.testimonials')

				@elseif($data['blocks'] == 'our-impact')

					@include('partials.our-impact')

				@elseif($data['blocks'] == 'why-us')

					@include('partials.why-us')

				@elseif($data['blocks'] == 'what-we-do')

					@include('partials.what-we-do')

				@elseif($data['blocks'] == 'sectors')

					@include('partials.sectors')

				@elseif($data['blocks'] == 'insights')

					@include('partials.insights')

				@elseif($data['blocks'] == 'partners')

					@include('partials.partners')

				@elseif($data['blocks'] == 'statistics')

					@include('partials.statistics')

				@elseif($data['blocks'] == 'challenges')

					@include('partials.challenges')

				@elseif($data['blocks'] == 'discovery')

					@include('partials.discovery')

				@elseif($data['blocks'] == 'trusted-by')

					@include('partials.trusted-by')

				@elseif($data['blocks'] == 'trusted-by-large')

					@include('partials.trusted-by-large')

				@endif

		    @endif
		@endforeach
	@endif
@endif