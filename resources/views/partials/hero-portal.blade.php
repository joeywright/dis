<div class="hero" data-viewport="detect" data-animate="fade">
	<div class="grid-container">
		<div class="grid-x grid-margin-x align-middle">
			<div class="large-7 medium-6 small-12 cell">
				@if(isset($fields['subheading']) && $fields['subheading'])
					<h5>{!! $fields['subheading'] !!}</h5>
				@endif
				<h1>{!! $title !!}</h1>
			</div>
			<div class="large-5 medium-6 small-12 cell align-self-bottom">
				@php
					$current_user_id = get_current_user_id();
					$current_user    = wp_get_current_user();
				@endphp

				@if(is_user_logged_in())
					@php $ad = get_field('advert', 'user_'. $current_user_id); @endphp
					@if($ad)
						<a href="<?= site_url(); ?>/contact"><img src="{{ $ad['url'] }}" class="advert hide-for-small-only"></a>
					@endif
				@endif
			</div>
		</div>
	</div>

	<div class="video">
		<video frameborder="0" allowfullscreen="" autoplay muted loop>
			  <source src="<?php echo get_stylesheet_directory_uri(); ?>/public/images/hero.mp4" type="video/mp4">
		</video>
	</div>
</div>