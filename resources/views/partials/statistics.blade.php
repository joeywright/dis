<section class="statistics panel secondary" data-viewport="detect" data-animate="fade" data-anchor="DIS In Numbers">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="large-12 text-center cell">
				@if($global['statistics'])
					<div class="standard-swiper swiper-container mt3">
					   	<div class="swiper-wrapper">
				       		@foreach ($global['statistics'] as $statistic)
								<div class="swiper-slide">
									<div class="grid-x grid-margin-x align-center text-center">
										<div class="large-7 medium-9 small-12 cell">
											<h5>DIS in numbers</h5>
							       			@if($statistic['text']){!! $statistic['text'] !!}@endif
							       			<a href="<?= site_url(); ?>/contact" class="button mt3">Get In Touch</a>
							       		</div>
						       		</div>
					       		</div>
							@endforeach
						</div>

						<div class="nav">
		                    <div class="prev"><svg width="47" height="23" viewBox="0 0 47 23" xmlns="http://www.w3.org/2000/svg"><g stroke="#fff" stroke-width="2" fill="none" fill-rule="evenodd"><path d="M0 11.501h29.124M25.012 1 44 11.501 25.012 22"/></g></svg></div>
		                    <div class="swiper-pagination show-for-small-only"></div>
		                    <div class="next"><svg width="47" height="23" viewBox="0 0 47 23" xmlns="http://www.w3.org/2000/svg"><g stroke="#fff" stroke-width="2" fill="none" fill-rule="evenodd"><path d="M0 11.501h29.124M25.012 1 44 11.501 25.012 22"/></g></svg></div>
		                </div>
					</div>
				@endif
			</div>
		</div>
	</div>
</section>