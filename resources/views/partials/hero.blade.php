<div class="hero" data-viewport="detect" data-animate="fade">
	<div class="grid-container">
		<div class="grid-x grid-margin-x align-middle">
			<div class="large-9 medium-10 small-12 cell">
				@if(isset($fields['subheading']) && $fields['subheading'])
					<h5>{!! $fields['subheading'] !!}</h5>
				@endif
				<h1>{!! $title !!}</h1>
			</div>
		</div>
	</div>

	<div class="video">
		<video frameborder="0" allowfullscreen="" autoplay muted loop>
			  <source src="<?php echo get_stylesheet_directory_uri(); ?>/public/images/hero.mp4" type="video/mp4">
		</video>
	</div>
</div>