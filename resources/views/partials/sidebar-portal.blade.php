<div class="sidebar">
	<ul>
		<li class="home @if(is_page('Portal')) active @endif"><a href="{{ home_url('/portal') }}">Home</a></li>
		<li class="tickets @if(is_page('Tickets')) active @endif"><a href="{{ home_url('/portal/tickets') }}">Tickets</a></li>
		<li class="log-ticket @if(is_page('Log A Ticket')) active @endif"><a href="{{ home_url('/portal/log-a-ticket') }}">Log a ticket</a></li>
		@php
			$member_level = SwpmMemberUtils::get_logged_in_members_level();
		@endphp

		@if($member_level == 2 || current_user_can('manage_options'))
			<li class="payments @if(is_page('Payments')) active @endif"><a href="{{ home_url('/portal/payments') }}">Payments</a></li>
		@endif
		<li class="services @if(is_page('Services')) active @endif"><a href="{{ home_url('/portal/services') }}">Your services</a></li>
		<li class="documents @if(is_page('Documents')) active @endif"><a href="{{ home_url('/portal/documents') }}">Documents</a></li>
		<li class="contact @if(is_page('Contact')) active @endif"><a href="{{ home_url('/portal/contact') }}">Contact</a></li>
	</ul>
</div>