<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	

  	<link rel="apple-touch-icon" sizes="180x180" href="@asset('/favicons/apple-touch-icon.png')">
    <link rel="icon" type="image/png" href="@asset('/favicons/favicon-32x32.png')" sizes="32x32">
    <link rel="icon" type="image/png" href="@asset('/favicons/favicon-16x16.png')" sizes="16x16">
    <link rel="mask-icon" href="@asset('/favicons/safari-pinned-tab.svg')" color="#5bbad5">
    <link rel="shortcut icon" href="@asset('/favicons/favicon.ico?v1')">
    <meta name="msapplication-config" content="@asset('/favicons/browserconfig.xml')">
    <meta name="theme-color" content="#ffffff">

    <!-- Hotjar Tracking Code for https://disnetwork.co.uk/ -->

<script>

  (function(h,o,t,j,a,r){
  
  h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
  
  h._hjSettings={hjid:5029436,hjsv:6};
  
  a=o.getElementsByTagName('head')[0];
  
  r=o.createElement('script');r.async=1;

  r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
  a.appendChild(r);
  })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
  
  </script>
  @php wp_head() @endphp
</head>