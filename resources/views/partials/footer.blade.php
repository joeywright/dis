
<footer>
	<div class="grid-container">
		<div class="grid-x grid-margin-x" id="accordion">
			<div class="large-3 medium-4 small-12 cell">
				<div class="hide-for-small-only">
					<h5>Solutions</h5>
					<ul class="mt3">
		        		{!! wp_nav_menu(['container' => 'ul', 'items_wrap' => '%3$s', 'menu' => 'Footer Menu 1', 'menu_class' => 'vertical menu', 'depth' => 1, 'walker' => new \App\Foundation_Walker()]) !!}
		        	</ul>
		        </div>
				<div class="accordion show-for-small-only">
					<div class="accordion-title">
			  			<p>Solutions</p>
			  		</div>

					<div class="accordion-content">
						<ul>
				  			{!! wp_nav_menu(['container' => 'ul', 'items_wrap' => '%3$s', 'menu' => 'Footer Menu 1', 'menu_class' => 'vertical menu', 'depth' => 1, 'walker' => new \App\Foundation_Walker()]) !!}
				  		</ul>
					</div>
				</div>
			</div>
			<div class="large-3 medium-4 small-12 cell">
				<div class="hide-for-small-only">
					<h5>Sectors</h5>
					<ul class="mt3">
		        		{!! wp_nav_menu(['container' => 'ul', 'items_wrap' => '%3$s', 'menu' => 'Footer Menu 2', 'menu_class' => 'vertical menu', 'depth' => 1, 'walker' => new \App\Foundation_Walker()]) !!}
		        	</ul>
		        </div>
				<div class="accordion show-for-small-only">
					<div class="accordion-title">
			  			<p>Sectors</p>
			  		</div>

					<div class="accordion-content">
						<ul>
				  			{!! wp_nav_menu(['container' => 'ul', 'items_wrap' => '%3$s', 'menu' => 'Footer Menu 2', 'menu_class' => 'vertical menu', 'depth' => 1, 'walker' => new \App\Foundation_Walker()]) !!}
				  		</ul>
					</div>
				</div>
			</div>
			<div class="large-3 medium-4 small-12 cell">
				<div class="hide-for-small-only">
					<h5>Quick links</h5>
					<ul class="mt3">
		        		{!! wp_nav_menu(['container' => 'ul', 'items_wrap' => '%3$s', 'menu' => 'Footer Menu 3', 'menu_class' => 'vertical menu', 'depth' => 1, 'walker' => new \App\Foundation_Walker()]) !!}
		        	</ul>
		        </div>
				<div class="accordion show-for-small-only">
					<div class="accordion-title">
			  			<p>Quick links</p>
			  		</div>

					<div class="accordion-content">
						<ul>
				  			{!! wp_nav_menu(['container' => 'ul', 'items_wrap' => '%3$s', 'menu' => 'Footer Menu 3', 'menu_class' => 'vertical menu', 'depth' => 1, 'walker' => new \App\Foundation_Walker()]) !!}
				  		</ul>
					</div>
				</div>
			</div>
			<div class="large-3 medium-4 small-12 cell">
				<div class="hide-for-small-only">
					<h5>Company Information</h5>
					<ul class="mt3">
		        		{!! wp_nav_menu(['container' => 'ul', 'items_wrap' => '%3$s', 'menu' => 'Footer Menu 4', 'menu_class' => 'vertical menu', 'depth' => 1, 'walker' => new \App\Foundation_Walker()]) !!}
		        	</ul>
		        </div>
				<div class="accordion show-for-small-only">
					<div class="accordion-title">
			  			<p>Company Information</p>
			  		</div>

					<div class="accordion-content">
						<ul>
				  			{!! wp_nav_menu(['container' => 'ul', 'items_wrap' => '%3$s', 'menu' => 'Footer Menu 4', 'menu_class' => 'vertical menu', 'depth' => 1, 'walker' => new \App\Foundation_Walker()]) !!}
				  		</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="lower-footer">
		<div class="grid-container">
			<div class="grid-x grid-margin-x align-middle">
				<div class="large-6 medium-6 small-12 cell">
					<p class="weather">
						@php date_default_timezone_set('Europe/London'); @endphp
						<?= date("g:i a"); ?> <span class="primary-text mr1 ml1">|</span>
						Cleckheaton –
						<span class="temp"></span> <span class="condition"></span>
					</p>

					<ul class="social">
						@if($contact['facebook'])<li><a href="{{ $contact['facebook'] }}" target="_blank"><img src="@asset('images/social/facebook.svg')" width="25" alt="Facebook Logo"></a></a></li>@endif
		        		@if($contact['twitter'])<li><a href="{{ $contact['twitter'] }}" target="_blank"><img src="@asset('images/social/twitter.svg')" width="25" alt="Twitter Logo"></a></li>@endif
		        		@if($contact['instagram'])<li><a href="{{ $contact['instagram'] }}" target="_blank"><img src="@asset('images/social/instagram.svg')" width="25" alt="Instagram Logo"></a></li>@endif
		        		@if($contact['linkedin'])<li><a href="{{ $contact['linkedin'] }}" target="_blank"><img src="@asset('images/social/linkedin.svg')" width="25" alt="LinkedIn Logo"></a></li>@endif
		        	</ul>
				</div>
				<div class="large-6 medium-6 small-12 author cell">
					<p>© <?= date('Y'); ?> Data Installation & Supplies Ltd <span class="primary-text mr1 ml1">|</span> Designed By <a href="https://smith.co.uk" target="_blank">Smith</a></p>
				</div>
			</div>
		</div>
	</div>
</footer>

</div>
