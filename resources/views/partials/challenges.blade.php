<section class="challenges"  data-viewport="detect" data-animate="fade" data-anchor="Challenges">
	<div class="grid-container">
		<div class="grid-x grid-margin-x align-bottom">
			@if($global['challenge_text_1'])
				<div class="large-5 medium-6 small-12 mb3-s cell">
					{!! $global['challenge_text_1'] !!}
				</div>
			@endif
			@if($global['challenge_text_2'])
				<div class="large-6 medium-6 small-12 cell">
					{!! $global['challenge_text_2'] !!}
				</div>
			@endif
		</div>
	</div>
</section>

@if($challenges)
	@foreach($challenges->posts as $challenge)
	 	@if($loop->iteration > 3) @break @endif

		@php
			$prefix = get_field('prefix', $challenge->ID);
			$question = get_field('question', $challenge->ID);
		@endphp
		<section class="two-col-layout" data-viewport="detect" data-animate="fade">
			<div class="grid-x grid-margin-x align-middle">
	    		@if($loop->iteration % 2 == 0)
	    			<div class="large-5 large-offset-1 medium-6 small-12 medium-order-1 small-order-2 half-grid right cell">
	    				@if($prefix && $question)
			  				<h1 class="primary-text">{!! $prefix !!}</h1>
			  				<h3>{!! $question !!}</h3>
			  			@else
			  				<h3>{!! $challenge->name !!}</h3>
			  			@endif
						<a href="@php echo get_permalink( $challenge->ID) @endphp" class="follow-link mt3">Read Our Solution</a>
	    			</div>
	    			{{-- Why img --}}
					<div class="large-6 medium-6 small-12 medium-order-2 small-order-1 cell">
						<img src="@php echo wp_get_attachment_url( get_post_thumbnail_id($challenge->ID)); @endphp" class="block-img">
	    			</div>
				@else
					<div class="large-6 medium-6 small-12 cell">
						<img src="@php echo wp_get_attachment_url( get_post_thumbnail_id($challenge->ID)); @endphp" class="block-img">
	    			</div>
	    			<div class="large-5 large-offset-1 medium-6 small-12 half-grid cell">
	    				@if($prefix && $question)
			  				<h1 class="primary-text">{!! $prefix !!}</h1>
			  				<h3>{!! $question !!}</h3>
			  			@else
			  				<h3>{!! $challenge->name !!}</h3>
			  			@endif
						<a href="@php echo get_permalink( $challenge->ID) @endphp" class="follow-link mt3">Read Our Solution</a>
	    			</div>
				@endif
			</div>
		</section>
		<hr class="m0">
	@endforeach
@endif