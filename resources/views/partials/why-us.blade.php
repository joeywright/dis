<section class="why-choose-us panel secondary" data-viewport="detect" data-animate="fade" data-anchor="Why Us">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="large-3 medium-6 small-12 cell">
				@if($global['why_us_text'])
					{!! $global['why_us_text'] !!}
				@endif
			</div>

			@if($global['why_us_icons'])
				<div class="large-8 large-offset-1 medium-6 small-12 cell">
					<div class="icons grid-x grid-margin-x hide-for-small-only small-up-1 medium-up-2 large-up-3 mt2">
						@foreach($global['why_us_icons'] as $key=>$item)
							<div class="cell small-p mt3">
								@if($item['icon'])
									<img src="{{ $item['icon']['url'] }}" width="70" alt="{{ $item['icon']['alt'] }}" class="mb3 mt1">
								@endif

								@if($item['text'])
									{!! $item['text'] !!}
								@endif
							</div>
						@endforeach
					</div>
					<div class="grid-x grid-margin-x show-for-small-only mt4">
			    		<div class="cell">
							<div class="standard-swiper swiper">
				           		<div class="swiper-wrapper">
				           			@foreach($global['why_us_icons'] as $key=>$item)
										<div class="swiper-slide text-center">
											@if($item['icon'])
												<img src="{{ $item['icon']['url'] }}" alt="{{ $item['icon']['alt'] }}" class="mb3 mt1" width="70">
											@endif

											@if($item['text'])
												{!! $item['text'] !!}
											@endif
										</div>
									@endforeach
				            	</div>
				            	<div class="nav">
		                           <div class="prev"><svg width="47" height="23" viewBox="0 0 47 23" xmlns="http://www.w3.org/2000/svg"><g stroke="#fff" stroke-width="2" fill="none" fill-rule="evenodd"><path d="M0 11.501h29.124M25.012 1 44 11.501 25.012 22"/></g></svg></div>
		                            <div class="swiper-pagination"></div>
		                            <div class="next"><svg width="47" height="23" viewBox="0 0 47 23" xmlns="http://www.w3.org/2000/svg"><g stroke="#fff" stroke-width="2" fill="none" fill-rule="evenodd"><path d="M0 11.501h29.124M25.012 1 44 11.501 25.012 22"/></g></svg></div>
		                        </div>
				            </div>
			            </div>
			        </div>
				</div>
			@endif
		</div>
	</div>
</section>