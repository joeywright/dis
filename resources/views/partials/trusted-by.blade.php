@if($global['brands'])
	<div class="logo-grid">
		<div class="grid-container">
			<div class="grid-x small-up-2 medium-up-4 large-up-6">
				<div class="cell">
					<h5>Trusted By</h5>
				</div>
				@foreach($global['brands'] as $partner)
					@if($loop->iteration > 5) @break @endif
					<div class="cell text-center">
						@if($partner['url'])
							<img src="{!! $partner['url'] !!}">
				  		@endif
					</div>
				@endforeach
			</div>
		</div>
	</div>
@endif