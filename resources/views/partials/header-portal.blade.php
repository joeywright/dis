
	<header class="portal-header">
		<div class="grid-container">
			<div class="grid-x grid-margin-x align-middle">
				<div class="large-3 medium-3 small-6 cell">
					<a href="{{ home_url('/') }}" class="logo">
						<img src="@asset('images/logo-white.svg')" width="200" alt="DIS Logo">
					</a>
				</div>
				<div class="large-9 medium-6 small-6 cell">
					<nav class="show-for-large" role="navigation">
						<ul class="menu">
							<li><a href="{{ home_url('/portal/log-a-ticket') }}" class="log-ticket">Log a ticket</a></li>
							<li><a href="{{ home_url('/portal/contact') }}" class="contact">Contact</a></li>
							<li><a href="{{ home_url('?swpm-logout=true') }}" class="log-out">Log Out</a></li>
			        	</ul>
					</nav>

					<a href="#" class="show-for-small-only dot-toggle">
						<span></span>
						<span></span>
						<span></span>

						<span></span>
						<span></span>
						<span></span>

						<span></span>
						<span></span>
						<span></span>
					</a>
				</div>
			</div>
		</div>

		<div class="overlay-menu hide-for-large">
			<ul class="menu vertical">
				<li><a href="{{ home_url('/portal/log-a-ticket') }}" class="log-ticket">Log a ticket</a></li>
				<li><a href="{{ home_url('/portal/contact') }}" class="contact">Contact</a></li>
				<li><a href="{{ home_url('?swpm-logout=true') }}" class="log-out">Log Out</a></li>
	    	</ul>
		</div>
	</header>

