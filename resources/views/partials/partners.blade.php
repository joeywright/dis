@if($global['partners'])
	<div class="logo-swiper-grid" data-viewport="detect" data-animate="fade">
		<div class="grid-container">
			<div class="grid-x grid-margin-x align-center">
				<div class="large-3 small-12 cell">
					<h5>Partnering with</h5>
				</div>
				<div class="large-9 small-12 cell">
					<div class="logo-swiper swiper swiper-container">
					   	<div class="swiper-wrapper">
				       		@foreach ($global['partners'] as $partner)
								<div class="swiper-slide">
									@if($partner['url'])
										<img src="{!! $partner['url'] !!}">
							  		@endif
					       		</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endif