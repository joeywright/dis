<section class="testimonials-wrap @if(isset($background)) @if($background) {{ $background }} panel top-curve @endif @endif" data-viewport="detect" data-animate="fade" data-anchor="Testimonials">
	<div class="grid-container">
		<div class="grid-x grid-margin-x align-center">
			<div class="large-8 medium-10 small-12 text-center cell">
				@if($global['testimonials_text'])
					{!! $global['testimonials_text'] !!}
				@endif
			</div>
		</div>
		@if($global['testimonials'])
			<div class="grid-x grid-margin-x align-center mt4">
				<div class="large-12 cell">
					<div class="testimonials-swiper swiper-container">
					   	<div class="swiper-wrapper">
				       		@foreach ($global['testimonials'] as $testimonial)
								<div class="swiper-slide">
									<div class="testimonial">
										<img src="@asset('images/speech.png')" width="42" class="mb2">
						       			@if($testimonial['text']){!! $testimonial['text'] !!}@endif
						       			@if($testimonial['name'])<h5 class="name">{!! $testimonial['name'] !!}</h5>@endif
						       		</div>
					       		</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
			<div class="grid-x grid-margin-x align-center mt4">
				<div class="large-8 medium-10 small-12 cell">
					<div class="testimonials-nav nav">
	                   	<div class="prev"><img src="@asset('images/arrow.svg')" width="50"></div>
	                   	<div class="swiper-pagination"></div>
						<div class="next"><img src="@asset('images/arrow.svg')" width="50"></div>
	                </div>
				</div>
			</div>
		@endif
	</div>
</section>