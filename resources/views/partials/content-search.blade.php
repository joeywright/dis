<div class="cell mb3">
	<a href="{{ get_permalink() }}" class="post">
		@if(get_the_post_thumbnail_url())
			<div class="image" style="background-image: url(@php echo get_the_post_thumbnail_url(); @endphp)"></div>
		@else
			<div class="image placeholder"></div>
		@endif

  		<h4 class="title mt2">{!! get_the_title() !!}</h4>
  		<p class="desc">@php echo wp_trim_words(get_the_content(), 20, '...'); @endphp</p>
		<div class="button">Read More</div>
	</a>
</div>