<div class="cell" @php post_class() @endphp>
	<a href="{{ get_permalink() }}" class="card">
		@if(get_the_post_thumbnail_url())
			<div class="image" style="background-image: url(@php echo get_the_post_thumbnail_url(); @endphp)"></div>
		@else
			<div class="image placeholder"></div>
		@endif
		<div class="content">
			<h4>{{ get_the_title() }}</h4>
			<div class="button">View More</div>
		</div>
	</a>
</div>
