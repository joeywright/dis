<section class="grey panel" data-viewport="detect" data-animate="fade" data-anchor="Our Impact">
	<div class="grid-container">
		@if($global['our_impact_text'])
			<div class="grid-x grid-margin-x mb4">
				<div class="large-8 medium-6 small-12 cell">
					{!! $global['our_impact_text'] !!}
				</div>
			</div>
		@endif

		@if($global['impact_boxes'])
			<div class="grid-x grid-margin-x hide-for-small-only small-up-1 medium-up-2 large-up-3">
				@foreach($global['impact_boxes'] as $key=>$box)
					<div class="cell">
						<div class="card">
							@if($box['icon'])
								<img src="{{ $box['icon']['url'] }}" width="60" alt="{{ $box['icon']['alt'] }}" class="mb3">
							@endif

							@if($box['text'])
								{!! $box['text'] !!}
							@endif
						</div>
					</div>
				@endforeach
			</div>

			<div class="grid-x grid-margin-x show-for-small-only">
	    		<div class="cell">
					<div class="standard-swiper swiper">
		           		<div class="swiper-wrapper">
		           			@foreach($global['impact_boxes'] as $key=>$box)
								<div class="swiper-slide">
									<div class="card">
										@if($box['icon'])
											<img width="60" src="{{ $box['icon']['url'] }}" alt="{{ $box['icon']['alt'] }}" class="mb3">
										@endif

										@if($box['text'])
											{!! $box['text'] !!}
										@endif
									</div>
								</div>
							@endforeach
		            	</div>
		            	<div class="nav">
		                    <div class="prev"><svg width="47" height="23" viewBox="0 0 47 23" xmlns="http://www.w3.org/2000/svg"><g stroke="#000" stroke-width="2" fill="none" fill-rule="evenodd"><path d="M0 11.501h29.124M25.012 1 44 11.501 25.012 22"/></g></svg></div>
                            <div class="swiper-pagination"></div>
                            <div class="next"><svg width="47" height="23" viewBox="0 0 47 23" xmlns="http://www.w3.org/2000/svg"><g stroke="#000" stroke-width="2" fill="none" fill-rule="evenodd"><path d="M0 11.501h29.124M25.012 1 44 11.501 25.012 22"/></g></svg></div>
                        </div>
		            </div>
	            </div>
	        </div>
		@endif
	</div>
</section>