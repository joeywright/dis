<section class="sectors panel secondary"  data-viewport="detect" data-animate="fade" data-anchor="Sectors">
	<div class="grid-container">
		<div class="grid-x grid-margin-x align-bottom">
			<div class="large-8 medium-6 small-12 cell">
				@if($global['sectors_text'])
					{!! $global['sectors_text'] !!}
				@endif
			</div>
		</div>
		@if($sectors)
			<div class="grid-x grid-margin-x small-up-1 medium-up-2 large-up-2 mt4">
				@foreach($sectors->posts as $sector)
					<div class="cell mb3">
						<a href="@php echo get_permalink( $sector->ID) @endphp" class="post">
							@php
								$icon = get_field('icon_white', $sector->ID);
							@endphp
			  				@if(wp_get_attachment_url( get_post_thumbnail_id($sector->ID)))
			  					<div class="image mb3" style="background-image: url('@php echo wp_get_attachment_url( get_post_thumbnail_id($sector->ID)); @endphp');">
			  						@if($icon)
			  							<div class="icon"><img width="60" src="{{ $icon['url'] }}"></div>
			  						@endif
			  					</div>
							@else
								<div class="image mb3 placeholder" style="background-image: url('@asset('images/logo.svg')');"></div>
							@endif

				  			<h3>{!! $sector->post_title !!}</h3>
				  			<p>@php echo wp_trim_words($sector->post_content, 50, '...'); @endphp</p>
							<div class="button">Find Out More</div>
						</a>
			       	</div>
				@endforeach
			</div>
		@endif
	</div>
</section>