<section class="grey panel side">
    <div class="grid-container">
	    <div class="grid-x grid-margin-x">
			<div class="large-12 small-12 cell">
				<h6>Explore</h6>
				<h1 class="push-down outline">Archive</h1>
			</div>
		</div>

		<div class="grid-x grid-margin-x mb4">
    		<div class="cell">
				<div class="card-swiper swiper">
	           		<div class="swiper-wrapper">
	           			@foreach ($archives['archives']->posts as $archive)
							<div class="swiper-slide">
								<a href="@php echo get_permalink( $archive->ID ) @endphp" class="card">
					  				@if(get_the_post_thumbnail_url($archive->ID))
										<div class="image" style="background-image: url(@php echo get_the_post_thumbnail_url($archive->ID) @endphp)"></div>
									@else
										<div class="image placeholder"></div>
									@endif
									<div class="content">
					  					<h4>{{ $archive->post_title }}</h4>
					  					<div class="button">Find Out More</div>
									</div>
								</a>
							</div>
						@endforeach
	            	</div>
	            	<div class="nav">
	                	<div class="prev"><img src="@asset('images/line-arrow-left.svg')" width="30"></div>
						<div class="next"><img src="@asset('images/line-arrow-right.svg')" width="30"></div>
	                </div>
	            </div>
            </div>
        </div>

        <a href="#" class="button">Explore Our Full Archive</a>
	</div>
</section>