@extends('layouts.app')

@section('content')
  	<div class="hero">
		<div class="caption">
			<div class="grid-container">
				<div class="grid-x grid-margin-x align-middle align-center">
					<div class="medium-12 small-12 cell">
						<h1>404 Page Not Found</h1>
					</div>
				</div>
			</div>
		</div>
	</div>


	@if (!have_posts())
		<section>
			<div class="grid-container">
				<div class="grid-x grid-margin-x align-middle">
					<div class="medium-8 small-12 cell">
					    <div class="alert alert-warning">
					      {{ __('Sorry, but the page you were trying to view does not exist.', 'sage') }}
					    </div>
					    {!! get_search_form(false) !!}
					</div>
				</div>
			</div>
		</section>
	@endif

	@include('partials.flexible-content')

	@include('partials.cta')

@endsection
