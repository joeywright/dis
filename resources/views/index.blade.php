@extends('layouts.app')

@section('content')

	@php $page_for_posts = get_option( 'page_for_posts' ); @endphp
	<div class="hero" data-viewport="detect" data-animate="fade">
		<div class="grid-container">
			<div class="grid-x grid-margin-x align-middle">
				<div class="large-9 medium-10 small-12 cell">
					<h5>The latest industry insights</h5>
					<h1>{!! $title !!}</h1>
				</div>
			</div>
		</div>
	</div>

	<section data-viewport="detect" data-animate="fade" data-anchor="All Insights">
        <div class="grid-container">
        	<div class="grid-x grid-margin-x">
				<div class="large-3 medium-3 small-12 cell">
					<h3>Categories</h3>

					@php $all_terms = get_categories(); @endphp
					@if($all_terms)
						<ul class="category-list mt3">
							@foreach($all_terms as $term)
								<li><a href="@php echo get_term_link($term) @endphp">{!! $term->name !!}</a></li>
							@endforeach
						</ul>
					@endif
				</div>
				<div class="large-8 large-offset-1 medium-9 small-12 cell">
					<div class="grid-x grid-margin-x medium-up-2 small-up-1" id="posts">
						@foreach($blog_posts->posts as $blog_post)
							@php
								the_post();
								$terms = get_the_category();
								$terms_string = join('<span style="color:#fff"> |</span> ', wp_list_pluck($terms, 'name'));
							@endphp
							<div class="post-item cell mb4">
								<a href="@php echo get_permalink( ) @endphp" class="post">
					  				@if(wp_get_attachment_url( get_post_thumbnail_id()))
					  					<div class="image mb3" style="background-image: url('@php echo wp_get_attachment_url( get_post_thumbnail_id()); @endphp');"></div>
									@else
										<div class="image mb3 placeholder" style="background-image: url('@asset('images/logo.svg')');"></div>
									@endif

									<h5>{!! $terms_string !!}</h5>
						  			<h4 class="mb2">{{ the_title() }}</h4>
						  			<p>@php echo wp_trim_words(get_the_content(), 20, '...'); @endphp</p>
									<div class="button">Read Insight</div>
								</a>
					       	</div>
						@endforeach
					</div>
					<div class="grid-x grid-margin-x align-center text-center">
						<div class="cell">
							<nav class="load-more text-center">
						    	<?php next_posts_link( 'Load More Posts' ); ?>
						    </nav>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	@include('partials.trusted-by-large')

@endsection