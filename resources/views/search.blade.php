@extends('layouts.app')

@section('content')

  	<div class="hero" data-viewport="detect" data-animate="fade">
		<div class="grid-container">
			<div class="grid-x grid-margin-x align-middle">
				<div class="medium-6 small-12 cell">
					<h1>Search Results</h1>
					<?php if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb( '<p class="breadcrumbs mt4">','</p>' );
					} ?>
				</div>
			</div>
		</div>
	</div>

	<section data-viewport="detect" data-animate="fade">
		<div class="grid-container">
			<div class="grid-x grid-margin-x small-up-1 medium-up-2 large-up-4">
				@if (!have_posts())
					<div class="alert alert-warning">
						{{ __('Sorry, no results were found.', 'sage') }}
					</div>
					{!! get_search_form(false) !!}
				@endif

				@while(have_posts()) @php the_post() @endphp
					@include('partials.content-search')
				@endwhile
			</div>
			<div class="grid-x grid-margin-x">
				<div class="large-12 cell">
  					{!! get_the_posts_navigation() !!}
  				</div>
  			</div>
		</div>
	</section>

	@include('partials.flexible-content')

	@include('partials.cta')

@endsection
