@extends('layouts.app')

@php
	$post = get_post();
	$terms = get_the_terms( $post->ID, 'service-categories' );
	$terms_string = join(', ', wp_list_pluck($terms, 'name'));
@endphp

@section('content')
   @while(have_posts()) @php the_post() @endphp

	<div class="hero" data-viewport="detect" data-animate="fade">
		<div class="grid-container">
			<div class="grid-x grid-margin-x align-middle">
				<div class="large-9 medium-10 small-12 cell">
					@if($terms_string)
						<h5>{!! $terms_string !!}</h5>
					@endif
					<h1>{!! $title !!}</h1>
				</div>
			</div>
			@if($fields['whitepaper'])
				<a href="#whitepaper" class="whitepaper-link button">Download Our Whitepaper</a>
			@endif
		</div>
	</div>

  	@if( '' !== get_post()->post_content )
		<section data-viewport="detect" data-animate="fade" data-anchor="Introduction">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
					<div class="large-8 medium-10 small-12 cell">
						@if($fields['intro_text'])
							{!! $fields['intro_text'] !!}
						@endif
					</div>
				</div>
				<div class="grid-x grid-margin-x mt4">
					<div class="large-6 large-offset-4 medium-8 small-12 cell">
						@php the_content(); @endphp
					</div>
				</div>
			</div>
		</section>
	@endif




	@if($fields['video'] || $fields['service_image'] || $fields['solution_text_block_1'] || $fields['solution_text_block_2'] || $fields['solution_text_block_3'])
		<section class="secondary gradient panel" @if($fields['video'] || $fields['service_image']) style="padding-top: 0;" @endif data-viewport="detect" data-animate="fade">
			@if($fields['video'] || $fields['service_image'])
				<div class="split">
			@endif
				<div class="grid-container">
					@if($fields['video'])
						<div class="grid-x grid-margin-x">
							<div class="small-12 cell">
								@php
									$video = get_field( 'video' );

									preg_match('/src="(.+?)"/', $video, $matches_url );
									$src = $matches_url[1];

									preg_match('/embed(.*?)?feature/', $src, $matches_id );
									$id = $matches_id[1];
									$id = str_replace( str_split( '?/' ), '', $id );
								@endphp


								<div class="video-block" style="background-image: url('http://img.youtube.com/vi/<?php echo $id; ?>/maxresdefault.jpg');" data-viewport="detect" data-animate="fade">
									<a data-open="video-block"><img src="@asset('images/play.svg')" width="80"></a>

									<div class="responsive-embed widescreen mb0 video">{!! $fields['video'] !!}</div>
								</div>
							</div>
						</div>
					@elseif(isset($fields['service_image']) && $fields['service_image'])
						<div class="grid-x grid-margin-x">
							<div class="small-12 cell">
								<img src="{{ $fields['service_image']['url'] }}" alt="{{ $fields['service_image']['alt'] }}">
							</div>
						</div>
					@endif
				@if($fields['video'] || $fields['service_image']) </div> @endif
			</div>
			<div class="grid-container">
				@if($fields['solution_text_block_1'])
					<div class="grid-x grid-margin-x mt4">
						<div class="large-8 medium-10 small-12 cell">
							{!! $fields['solution_text_block_1'] !!}
						</div>
					</div>
				@endif

				@if($fields['solution_text_block_2'] || $fields['solution_text_block_3'])
					<div class="grid-x grid-margin-x mt4">
						@if($fields['solution_text_block_2'])
							<div class="large-6 medium-6 small-12 cell">
								{!! $fields['solution_text_block_2'] !!}
							</div>
						@endif
						@if($fields['solution_text_block_3'])
							<div class="large-6 medium-6 small-12 cell">
								{!! $fields['solution_text_block_3'] !!}
							</div>
						@endif
					</div>
				@endif
			</div>
		</section>
	@endif

	@include('partials.flexible-content')

	<section data-viewport="detect" data-animate="fade" data-anchor="Related Services">
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="large-8 medium-9 small-12 cell">
					<h5>{!! $title !!}</h5>
					<h1>Related Services</h1>
				</div>
			</div>

			@php
				$args = array(
		            'post_type' => 'service',
		            'posts_per_page' => 6,
		            'orderby' => 'menu_order title',
		            'order' => 'ASC',
		            'post__not_in' => array($post->ID),
		            'tax_query' => array(
                        array(
	                        'taxonomy' => 'service-categories',
	                        'field' => 'term_id',
	                        'terms' => $terms[0]->term_id,
                        )
                    )
		        );
		        $related = new WP_Query( $args );
         	@endphp

			@if($related)
				<div class="grid-x grid-margin-x mt4">
					<div class="large-12 cell">
						<div class="card-swiper swiper-container">
							<div class="swiper-wrapper">
								@foreach($related->posts as $related_service)
									<div class="swiper-slide">
										<a href="@php echo get_permalink( $related_service->ID) @endphp" class="post btn-align">
											@php
												$icon = get_field('icon_white', $related_service->ID);
											@endphp
							  				@if(wp_get_attachment_url( get_post_thumbnail_id($related_service->ID)))
							  					<div class="image mb3" style="background-image: url('@php echo wp_get_attachment_url( get_post_thumbnail_id($related_service->ID)); @endphp');">
							  						@if($icon)
							  							<div class="icon"><img src="{{ $icon['url'] }}"></div>
							  						@endif
							  					</div>
											@else
												<div class="image mb3 placeholder" style="background-image: url('@asset('images/logo.svg')');"></div>
											@endif

								  			<h3 class="primary-text">{!! $related_service->post_title !!}</h3>
											<div class="button post-btn ">Find Out More</div>
										</a>
							       	</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			@endif
		</div>
	</section>

	@if($fields['whitepaper'])
		<section data-viewport="detect" data-animate="fade" id="whitepaper" data-anchor="Whitepaper">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
					<div class="cell">
						<div class="whitepaper">
							<div class="image">
								<img src="@asset('images/whitepaper-icon.svg')" class="icon" width="60">

								<div class="whitepaper-mockup">
									<!--<div class="mockup-text">
										@if($terms_string)
											<h5>{!! $terms_string !!}</h5>
										@endif
										<div class="title">{!! $title !!}</div>
									</div> -->
									<img src="@asset('images/whitepaper.png')">
								</div>
							</div>
							<div class="content">
								<h1 class="mb3">Download Our Whitepaper</h1>
								{!! do_shortcode('[gravityform id="2" title="false" description="false" ajax="true" field_values="whitepaper='.$fields['whitepaper']['url'].'"]') !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	@endif

	@include('partials.cta')

	<section data-viewport="detect" data-animate="fade" data-anchor="Insights">
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="large-8 medium-9 small-12 cell">
					@if($terms_string)
						<h5>{!! $terms_string !!}</h5>
					@endif
					<h1>{!! $title !!} <br> Insights</h1>
				</div>
			</div>

			@php
				$related_insights = get_field('related_insights');

				if($related_insights):
					$blog_posts->posts = $related_insights;
				else:
					$args = array(
			            'post_type' => 'post',
			            'posts_per_page' => 10,
			            'orderby' => 'menu_order title',
			            'order' => 'ASC',
			        );
			        $blog_posts = new WP_Query( $args );
	         	endif;
	         @endphp

			@if($blog_posts)
				<div class="grid-x grid-margin-x mt4">
					<div class="large-12 cell">
						<div class="card-swiper swiper-container">
							<div class="swiper-wrapper">
								@foreach($blog_posts->posts as $blog_post)
									<div class="swiper-slide">
										<a href="@php echo get_permalink( $blog_post->ID) @endphp" class="post card">
											@php
												$icon = get_field('icon_white', $blog_post->ID);
											@endphp
							  				@if(wp_get_attachment_url( get_post_thumbnail_id($blog_post->ID)))
							  					<div class="image" style="background-image: url('@php echo wp_get_attachment_url( get_post_thumbnail_id($blog_post->ID)); @endphp');">
							  						@if($icon)
							  							<div class="icon"><img src="{{ $icon['url'] }}"></div>
							  						@endif
							  					</div>
											@else
												<div class="image placeholder" style="background-image: url('@asset('images/logo.svg')');"></div>
											@endif
											<div class="content">
												<h6><time class="updated" datetime="{{ get_post_time('c', true) }}">{{ get_the_date('d/m/Y') }}</time></h6>
									  			<h3>{!! $blog_post->post_title !!}</h3>
									  			<p>@php echo wp_trim_words($blog_post->post_content, 20, '...'); @endphp</p>
												<div class="follow-link">Read Insight</div>
											</div>
										</a>
							       	</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			@endif
		</div>
	</section>

  	@endwhile

@endsection
