@extends('layouts.app')

@section('content')

	@php
		$category = get_queried_object();
		$intro_text = get_field('intro_text', $category->taxonomy . '_' . $category->term_id);
		$desc_1 = get_field('category_description_1', $category->taxonomy . '_' . $category->term_id);
		$desc_2 = get_field('category_description_2', $category->taxonomy . '_' . $category->term_id);
	@endphp

	<div class="hero" data-viewport="detect" data-animate="fade">
		<div class="grid-container">
			<div class="grid-x grid-margin-x align-middle">
				<div class="large-9 medium-10 small-12 cell">
					@if(isset($fields['subheading']))
						<h5>{!! $fields['subheading'] !!}</h5>
					@endif
					<h1>{!! single_term_title() !!}</h1>
				</div>
			</div>
		</div>
	</div>


	@if($intro_text || the_archive_description())
		<section data-viewport="detect" data-animate="fade">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
					<div class="large-8 medium-10 small-12 cell">
						@php the_archive_description(); @endphp
					</div>
				</div>
				@if($intro_text)
					<div class="grid-x grid-margin-x mt4">
						<div class="large-6 large-offset-4 medium-8 small-12 cell">
							{!! $intro_text !!}
						</div>
					</div>
				@endif
			</div>
		</section>
	@endif

    <section class="secondary gradient panel" data-viewport="detect" data-animate="fade">
        <div class="grid-container">
			<div class="grid-x grid-margin-x align-middle">
				<div class="large-9 medium-10 small-12 cell">
					<h1>{!! single_term_title() !!}<br> Solutions</h1>
				</div>
			</div>
			<div class="grid-x grid-margin-x large-up-3 medium-up-2 small-up-1">
				@while (have_posts()) @php the_post() @endphp
					<div class="cell mt4">
						<a href="@php echo get_permalink( ) @endphp" class="post">
			  				@php
								$icon = get_field('icon_white');
							@endphp
			  				@if(wp_get_attachment_url( get_post_thumbnail_id()))
			  					<div class="image mb3" style="background-image: url('@php echo wp_get_attachment_url( get_post_thumbnail_id()); @endphp');">
			  						@if($icon)
			  							<div class="icon"><img src="{{ $icon['url'] }}"></div>
			  						@endif
			  					</div>
							@else
								<div class="image mb3 placeholder" style="background-image: url('@asset('images/logo.svg')');"></div>
							@endif

				  			<h3 class="mb2">{{ the_title() }}</h3>
				  			<p>@php echo wp_trim_words(get_the_content(), 20, '...'); @endphp</p>
							<div class="button">Find Out More</div>
						</a>
			       	</div>
				@endwhile
			</div>
			<div class="grid-x grid-margin-x align-center text-center">
				<div class="cell">
					{!! the_posts_pagination() !!}
				</div>
			</div>
		</div>
	</section>

	@include('partials.partners')

	<hr class="m0">

	@if($desc_1 || $desc_2)
		<section data-viewport="detect" data-animate="fade">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
					@if($desc_1)
						<div class="large-6 medium-6 small-12 cell">
							{!! $desc_1 !!}
						</div>
					@endif
					@if($desc_2)
						<div class="large-6 medium-6 small-12 cell">
							{!! $desc_2 !!}
						</div>
					@endif
				</div>
			</div>
		</section>
	@endif

	<section class="grey panel" data-viewport="detect" data-animate="fade">
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="large-6 medium-9 small-12 cell">
					<h5>Learn more</h5>
					<h1>{!! single_term_title() !!}<br> Insights</h1>
				</div>
			</div>
			@if($blog_posts)
				<div class="grid-x grid-margin-x mt4">
					<div class="large-12 cell">
						<div class="card-swiper swiper-container">
							<div class="swiper-wrapper">
								@foreach($blog_posts->posts as $post)
									<div class="swiper-slide">
										<a href="@php echo get_permalink( $post->ID) @endphp" class="post card">
											@php
												$icon = get_field('icon_white', $post->ID);
											@endphp
							  				@if(wp_get_attachment_url( get_post_thumbnail_id($post->ID)))
							  					<div class="image" style="background-image: url('@php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID)); @endphp');">
							  						@if($icon)
							  							<div class="icon"><img src="{{ $icon['url'] }}"></div>
							  						@endif
							  					</div>
											@else
												<div class="image placeholder" style="background-image: url('@asset('images/logo.svg')');"></div>
											@endif
											<div class="content">
												<h6><time class="updated" datetime="{{ get_post_time('c', true) }}">{{ get_the_date('d/m/Y') }}</time></h6>
									  			<h3>{!! $post->post_title !!}</h3>
									  			<p>@php echo wp_trim_words($post->post_content, 20, '...'); @endphp</p>
												<div class="follow-link">Read Insight</div>
											</div>
										</a>
							       	</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			@endif
		</div>
	</section>

	@include('partials.cta')

@endsection