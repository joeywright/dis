@extends('layouts.app')

@php
	$post = get_post();
	$terms = get_the_terms( $post->ID, 'challenge-categories' );
@endphp

@section('content')
   @while(have_posts()) @php the_post() @endphp

	<div class="hero" data-viewport="detect" data-animate="fade">
		<div class="grid-container">
			<div class="grid-x grid-margin-x align-middle">
				<div class="large-12 cell">
					<h5>Sectors</h5>
					<h1>{!! $title !!}</h1>
				</div>
			</div>
		</div>
		@if($featured_image)
			<div class="split bottom">
				<div class="grid-container">
					<div class="grid-x grid-margin-x align-middle">
						<div class="large-12 cell">
								@php $link = get_field('external_link'); @endphp

							   @if($link) <a href="{{ $fields['external_link']['url'] }}">@endif
							    	<img src="{{ $featured_image }}">
							    @if($link)</a>@endif






						</div>
					</div>
				</div>
			</div>
		@endif
	</div>


	@if( '' !== get_post()->post_content )
		<section data-viewport="detect" data-animate="fade" data-anchor="Introduction">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
					<div class="large-6 medium-8 small-12 cell">
						@if($fields['intro_text'])

							{!! $fields['intro_text'] !!}
						@endif
					</div>
				</div>
				<div class="grid-x grid-margin-x mt4">
					<div class="large-6 large-offset-4 medium-8 small-12 cell">
						@php the_content(); @endphp
					</div>
				</div>
			</div>
		</section>
	@endif

	@include('partials.flexible-content')

	<section class="grey panel" data-viewport="detect" data-animate="fade" data-anchor="Challenges">
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="large-6 medium-8 small-12 cell">
					<h5>CASE STUDIES</h5>
					<h1>{!! $title !!} <br> Challenges</h1>
				</div>
			</div>
			@if($challenges)
				<div class="grid-x grid-margin-x mt4">
					<div class="large-12 cell">
						<div class="card-swiper challenges-swiper swiper-container">
							<div class="swiper-wrapper">
								@foreach($challenges->posts as $challenge)
									<div class="swiper-slide">
										<a href="@php echo get_permalink( $challenge->ID) @endphp" class="post card">
											@php
												$icon = get_field('icon_white', $challenge->ID);
											@endphp
							  				@if(wp_get_attachment_url( get_post_thumbnail_id($challenge->ID)))
							  					<div class="image" style="background-image: url('@php echo wp_get_attachment_url( get_post_thumbnail_id($challenge->ID)); @endphp');">
							  						@if($icon)
							  							<div class="icon"><img src="{{ $icon['url'] }}"></div>
							  						@endif
							  					</div>
											@else
												<div class="image placeholder" style="background-image: url('@asset('images/logo.svg')');"></div>
											@endif
											<div class="content">
									  			<h3>{!! $challenge->post_title !!}</h3>
									  			<p>@php echo wp_trim_words($post->post_content, 20, '...'); @endphp</p>
												<div class="follow-link">View Challenge</div>
											</div>
										</a>
							       	</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			@endif
		</div>
	</section>

	@include('partials.testimonials')

	@include('partials.cta')


  	@endwhile

@endsection
