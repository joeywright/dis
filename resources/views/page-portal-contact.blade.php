{{--
  Template Name: Portal – Contact
--}}
@extends('layouts.portal')

@section('content')
  @while(have_posts()) @php the_post() @endphp

	@include('partials.hero-portal')

	<section data-viewport="detect" data-animate="fade" data-anchor="Introduction">
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="large-3 medium-4 small-12 cell">
					@include('partials.sidebar-portal')
				</div>
				<div class="large-8 large-offset-1 medium-8 small-12 cell">
					@if( '' !== get_post()->post_content )
						@php the_content(); @endphp
					@endif

					<div class="grid-x grid-margin-x mt3">
						<div class="large-6 medium-6 small-12 mb3-s cell">
							<h3>Your Dedicated <br>Account Manager</h3>
							@php $am = get_field('account_manager', 'user_'. get_current_user_id()); @endphp

							@if($am)
								@if($am['email'])
									@if($am['name'])
										<p class="name">{{ $am['name'] }}</p>
									@endif
									@if($am['email'])
										<p class="email"><a href="mailto:{{ $am['email'] }}">{{ $am['email'] }}</a></p>
									@endif
									@if($am['phone'])
										<p class="phone"><a href="tel:{{ $am['phone'] }}">{{ $am['phone'] }}</a></p>
									@endif
									@if($am['hours'])
										<p class="hours">{{ $am['hours'] }}</p>
									@endif
								@endif
							@else
								<p>Your account manager will be assigned soon.</p>
							@endif
						</div>
						<div class="large-6 medium-6 small-12 cell">
							<h3>DIS Contact <br>Information</h3>
							@if($contact['email'])
								<p class="email"><a href="mailto:{{ $contact['email'] }}">{{ $contact['email'] }}</a></p>
							@endif
							@if($contact['phone'])
								<p class="phone"><a href="tel:{{ $contact['phone'] }}">{{ $contact['phone'] }}</a></p>
							@endif
							@if($contact['address'])
								<p class="address">{!! $contact['address'] !!}</p>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	@include('partials.flexible-content')

  @endwhile
@endsection
