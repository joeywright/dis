{{--
  Template Name: Portal – Login Form
--}}
@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

	@include('partials.hero')

	@if (is_user_logged_in()) {
		@php wp_redirect( site_url()."/portal", 301 ); @endphp
	@else
		<section data-viewport="detect" data-animate="fade">
			<div class="grid-container">
				<div class="grid-x grid-margin-x align-center">
					<div class="large-6 medium-8 small-12 cell">
						<script type="text/javascript">
							jQuery(function($) {
								$('.digits_login').keypress(function (e) {
									var key = e.which;
									if(key == 13) {
										$('#dig_login_va_otp').click();
										return false;  
									}
								});   
							});
						</script>
						@if( '' !== get_post()->post_content )
							@php the_content(); @endphp
						@endif
					</div>
				</div>
			</div>
		</section>
	@endif

	@include('partials.flexible-content')

  @endwhile
@endsection
